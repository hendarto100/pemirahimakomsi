<?php $this->load->view('headerAdmin');?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/img/fotoAdmin/<?php echo $foto?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $nama ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menu Navigasi</li>
            <li>  
              <a href="<?=base_url()?>CAdmin">
                <i class="fa fa-hand-pointer-o"></i>
                <span>Hak Akses</span>
              </a>
            </li>
            <li class="treeview active">
              <a href="#">
                <i class="fa fa-list-alt"></i> <span>Data Pemilih</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>CAdmin/hal_data_pemilih"><i class="fa fa-align-justify"></i>Semua data</a></li>
                <li><a href="<?=base_url()?>CAdmin/hal_data_pemilih_belum_teregistrasi"><i class="fa fa-user-times"></i>Belum Teregistrasi</a></li>
                <li class="active"><a href="<?=base_url()?>CAdmin/hal_data_pemilih_teregistrasi"><i class="fa fa-user-plus"></i>Teregistrasi</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Pemilih Teregistrasi
          </h1>
          <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Data Pemilih Teregistrasi</li>
          </ol>
        </section>
        </section>
        <!-- Main content -->
        <section class="content">
        	<div class="row">
            <div class="form-group" style="margin-left:1.5%;">
              <label>Pilih tahun pemira : </label>
              <form action="<?=base_url()?>CAdmin/hal_data_pemilih_teregistrasi" method="POST">
                <div class="input-group input-group-sm" style="width:20%;">
                  <select name="tahun" id="tahun" class="form-control">
                    <?php
                      foreach ($tahunPemira->result() as $value) {
                    ?>
                      <option value="<?php echo $value->tahun?>" <?php if($value->tahun == $tahun){echo "selected";} ?>><?php echo $value->tahun?></option>
                    <?php
                      }
                    ?>
                  </select>
                  <span class="input-group-btn">
                    <button class="btn btn-info btn-flat" type="submit">Tampil</button>
                  </span>
                </div>
              </form>
            </div><!-- /.form-group -->
            <br>
	            <div class="col-xs-12">
	            	<div class="box">
		                <div class="box-body">
		                  <table class="table table-bordered table-striped table-data-pemilih">
		                    <thead>
		                      <tr>
		                        <th>No</th>
                            <th>NIF</th>
                            <th>Nama</th>
                            <th>Angkatan</th>
                            <th>Status Mahasiswa</th>
                            <th>Status Pilih</th>
                            <th>Tahun Pemira</th>
		                      </tr>
		                    </thead>
		                  </table>
		                </div><!-- /.box-body -->
	              	</div><!-- /.box -->
	            </div><!-- /.col -->
        	</div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; Komputer dan Sistem Informasi 2013.</strong>
      </footer>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url()?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?=base_url()?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?=base_url()?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url()?>assets/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url()?>assets/dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript" language="javascript" class="init">
       // ajax data tables
       $(".table-data-pemilih").DataTable({
            ordering: false,
            processing: true,
            serverSide: true,
            ajax: {
              url: "<?=base_url()?>CAdmin/getDataPemilihTeregistrasi/<?php echo $tahun ?>",
              type:'POST',
            }
        });
    </script>
  </body>
</html>