<?php $this->load->view('headerSuperAdmin');?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/img/fotoAdmin/<?php echo $foto?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $nama ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menu Navigasi</li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin">
                <i class="fa fa-hand-pointer-o"></i>
                <span>Hak Akses</span>
              </a> 
            </li>
            <li class="treeview active">
              <a href="#">
                <i class="fa fa-list-alt"></i> <span>Data Pemilih</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih"><i class="fa fa-align-justify"></i>Semua data</a></li>
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih_belum_teregistrasi"><i class="fa fa-user-times"></i>Belum Teregistrasi</a></li>
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih_teregistrasi"><i class="fa fa-user-plus"></i>Teregistrasi</a></li>
              </ul>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_admin">
                <i class="fa fa-list"></i>
                <span>Data Admin</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_calon_ketua">
                <i class="fa fa-users"></i>
                <span>Data Calon Ketua</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_pemira">
                <i class="fa fa-calendar-minus-o"></i>
                <span>Data Pemira</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_perolehan_suara">
                <i class="fa fa-bar-chart"></i>
                <span>Perolehan Suara</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_riwayat_pemira">
                <i class="fa fa-line-chart"></i>
                <span>Riwayat Pemira</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Pemilih Pemira HIMAKOMSI
          </h1>
          <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Data Pemilih</li>
          </ol>
        </section>
        
        <section class="content-header">
          <div>
            <?php 
              if($this->session->flashdata('berhasil')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Ubah data <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('berhasilUbahStatusMahasiswa')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('pesanBerhasilUbahStatus'); ?>
                </div>
            <?php
              }else if($this->session->flashdata('nifAda')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Ubah data <strong>gagal !</strong> NIF sudah ada
                </div>
            <?php
              }else if($this->session->flashdata('suksestambah')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Tambah data pemilih <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('gagaltambah')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Tambah data pemilih <strong>gagal !</strong> NIF sudah ada
                </div>
            <?php
              }else if($this->session->flashdata('berhasilHapusPemilih')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Hapus data <strong>berhasil !</strong>
                </div>
            <?php
              }
            ?>
          </div>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              
              <div class="box">
                <div class="box-body">
                  <table class="table table-bordered table-striped table-data-pemilih">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>NIF</th>
                        <th>Nama</th>
                        <th>Angkatan</th>
                        <th>Status Mahasiswa</th>
                        <th>Hak Akses</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                  </table>
                  <a type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah"><i class="glyphicon glyphicon-plus"></i> Tambah Pemilih</a>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <!-- /.modalTambah -->
        <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Data Pemilih</h4>
              </div>
              <div class="modal-body">
                <form method="post" action="<?=base_url()?>CSuperAdmin/tambahDataPemilih" data-toggle="validator">
                  <div class="form-group">
                    <label>NIF</label>
                    <input type="number" class="form-control" name="nif" placeholder="NIF" required autofocus>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="nama" placeholder="Nama" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Angkatan</label>
                    <input type="number" class="form-control" name="angkatan" placeholder="Angkatan" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Status Mahasiswa</label>
                    <select class="form-control" name="status_mahasiswa">
                      <option value="Aktif" >Aktif</option>
                      <option value="Tidak Aktif" >Tidak Aktif</option>
                    </select>
                  </div><!-- /.form-group -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                  </div>         
                </form>
              </div>          
            </div>
          </div>
        </div>

        <!-- modal hapus -->
        <div class="modal fade" id="myModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" id="modal-content-hapus">
                    
                </div>
            </div>
        </div>

        <!-- modal ubah -->
        <div class="modal fade" id="modalUbah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content" id="modal-ubah">
                  
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>



      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; Komputer dan Sistem Informasi 2013.</strong>
      </footer>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url()?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?=base_url()?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?=base_url()?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url()?>assets/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url()?>assets/dist/js/demo.js"></script>
    <!-- page script -->
    <script src="<?=base_url()?>assets/js/validator.js"></script>

    <script type="text/javascript" language="javascript" class="init">
       // ajax data tables
       $(".table-data-pemilih").DataTable({
            ordering: false,
            processing: true,
            serverSide: true,
            ajax: {
              url: "<?php echo base_url('CSuperAdmin/getDataPemilih') ?>",
              type:'POST',
            }
        });

       //ajax modal hapus
       $(function(){
            $(document).on('click','.hapus-data',function(e){
                e.preventDefault();
                $("#myModalHapus").modal('show');
                $.post("<?php echo base_url('CSuperAdmin/modalHapusPemilih') ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $("#modal-content-hapus").html(html);
                    }   
                );
            });
        });

       //ajax modal edit
       $(function(){
            $(document).on('click','.edit-data',function(e){
                e.preventDefault();
                $("#modalUbah").modal('show');
                $.post("<?php echo base_url('CSuperAdmin/modalEditPemilih') ?>",
                    {id:$(this).attr('data-id')},
                    function(html){
                        $("#modal-ubah").html(html);
                    }   
                );
            });
        });

    </script>
  </body>
</html>