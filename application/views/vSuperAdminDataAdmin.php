<?php $this->load->view('headerSuperAdmin');?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/img/fotoAdmin/<?php echo $foto?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $nama ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menu Navigasi</li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin">
                <i class="fa fa-hand-pointer-o"></i>
                <span>Hak Akses</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-list-alt"></i> <span>Data Pemilih</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih"><i class="fa fa-align-justify"></i>Semua data</a></li>
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih_belum_teregistrasi"><i class="fa fa-user-times"></i>Belum Teregistrasi</a></li>
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih_teregistrasi"><i class="fa fa-user-plus"></i>Teregistrasi</a></li>
              </ul>
            </li>
            <li class="active">
              <a href="<?=base_url()?>CSuperAdmin/hal_data_admin">
                <i class="fa fa-list"></i>
                <span>Data Admin</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_calon_ketua">
                <i class="fa fa-users"></i>
                <span>Data Calon Ketua</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_pemira">
                <i class="fa fa-calendar-minus-o"></i>
                <span>Data Pemira</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_perolehan_suara">
                <i class="fa fa-bar-chart"></i>
                <span>Perolehan Suara</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_riwayat_pemira">
                <i class="fa fa-line-chart"></i>
                <span>Riwayat Pemira</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Admin Pemira HIMAKOMSI
          </h1>
          <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Data Admin</li>
          </ol>
        </section>
        
        <section class="content-header">
          <div>
            <?php 
              if($this->session->flashdata('berhasil')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Ubah data <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('nifAda')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Ubah data <strong>gagal !</strong> NIF sudah ada
                </div>
            <?php
              }else if($this->session->flashdata('namaPenggunaAdaUbah')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Ubah data <strong>gagal !</strong> Nama pengguna sudah ada
                </div>
            <?php
              }else if($this->session->flashdata('namaPenggunaAda')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Tambah data admin <strong>gagal !</strong> Nama pengguna sudah ada
                </div>
            <?php
              }else if($this->session->flashdata('suksestambah')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Tambah ada admin <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('gagaltambah')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Tambah data admin <strong>gagal !</strong> Admin sudah ada
                </div>
            <?php
              }else if($this->session->flashdata('uploadFotoGagal')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $error;?>
                </div>
            <?php
              }else if($this->session->flashdata('ubahFoto')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Ubah <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('berhasilHapusAdmin')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Hapus data <strong>berhasil !</strong>
                </div>
            <?php
              }
            ?>
          </div>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>NIF</th>
                        <th>Nama</th>
                        <th>Nama Pengguna</th>
                        <th>Angkatan</th>
                        <th>No HP</th>
                        <th>Level</th>
                        <th>Foto</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                    	<?php
                    		$i=0;
                    		foreach ($admin->result() as $row) {
                          $i++;
                    	?>
	                      <tr>
	                        <td><?php echo $i;?></td>
	                        <td><?php echo $row->nif;?></td>
	                        <td><?php echo $row->nama;?></td>
	                        <td><?php echo $row->nama_pengguna;?></td>
	                        <td><?php echo $row->angkatan;?></td>
	                        <td><?php echo $row->no_hp;?></td>
                          <td><?php echo $row->level;?></td>
                          <td>
                            <a data-toggle="modal" data-target="#myFoto<?php echo $row->nif?>"><img src="<?=base_url()?>assets/img/fotoAdmin/<?php echo $row->foto;?>" class="img-circle" style="height:20%;"></a>
                            <a data-toggle="modal" data-target="#myUbahFoto<?php echo $row->nif?>"> &nbsp;&nbsp;&nbsp;&nbsp;Ubah foto</a> 
                          </td>
	                        <td>
	                        	<a type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal<?php echo $row->nif?>"><i class="glyphicon glyphicon-pencil"></i>&nbsp; Ubah</a>
                            <?php if($row->nif != $nif){ ?>
                            <a type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapusAdmin<?php echo $row->nif?>"><i class="glyphicon glyphicon-trash"></i>&nbsp; Hapus</a>
                            <?php } ?>
	                        </td>
	                      </tr>
	                    <?php
	                    	}
	                    ?>
                  </table>
                  <a type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah"><i class="glyphicon glyphicon-plus"></i> Tambah Admin</a>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <!-- /.modalTambah -->
        <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Data Admin</h4>
              </div>
              <div class="modal-body">
                <?php 
                  $attributes = array('data-toggle'=>'validator');
                  echo form_open_multipart('CSuperAdmin/tambahDataAdmin',$attributes); 
                ?>
                  <div class="form-group">
                    <label>NIF</label>
                    <input type="number" class="form-control" name="nif" id="nif" placeholder="NIF" onkeyup="myFunction()" required autofocus>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="namaTampil" id="nama" disabled placeholder="Nama" required>
                    <input type="hidden" class="form-control" name="nama" id="nama1" placeholder="Nama" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Angkatan</label>
                    <input type="number" class="form-control" name="angkatan" id="angkatan" disabled placeholder="Angkatan" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Nama Pengguna</label>
                    <input type="text" class="form-control" name="nama_pengguna" placeholder="Nama Pengguna" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Kata sandi :</label>
                    <input type="password" class="form-control" data-minlength="6" name="kata_sandi" id="inputPassword" placeholder="Kata sandi" required>
                    <span class="help-block">Minimal 6 karakter</span>
                    <div style="margin-top:10px;"></div>
                    <input type="password" class="form-control" name="rekata_sandi" data-match="#inputPassword" data-match-error="Whoops, sandi tidak cocok" placeholder="Ulangi kata sandi" required>
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <label>No HP</label>
                    <input type="number" class="form-control" name="no_hp" placeholder="No HP" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Level</label>
                    <select class="form-control" name="level">
                      <option value="Super Admin"> Super Admin</option>
                      <option value="Admin"> Admin</option>
                    </select>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Foto</label>
                    <input type="file" name="userfile" size="20" class="form-control"/>
                    <span class="help-block">Maks dimensi 160 x 160. Tipe file jpeg | jpg | png</span>
                  </div><!-- /.form-group -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                  </div>         
                <!-- </form> -->
                <?php echo form_close();?>
              </div>          
            </div>
          </div>
        </div>

      <!-- modal ubah data -->
      <?php
        $i=0;
        foreach ($admin->result() as $row) {
        $i++;
      ?>
        
        <div class="modal fade" id="myModal<?php echo $row->nif?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ubah Data Admin</h4>
              </div>
              <div class="modal-body">
                <form method="post" action="<?=base_url()?>CSuperAdmin/ubahDataAdmin" data-toggle="validator">
                  <input type="hidden" name="nif" value="<?php echo $row->nif?>">
                  <input type="hidden" name="nama_pengguna_awal" value="<?php echo $row->nama_pengguna;?>">
                  <div class="form-group">
                    <label>NIF</label>
                    <input type="number" class="form-control" value="<?php echo $row->nif?>" name="nif" disabled placeholder="NIF" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" value="<?php echo $row->nama?>" name="nama" disabled placeholder="Nama" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Angkatan</label>
                    <input type="number" class="form-control" value="<?php echo $row->angkatan?>" disabled name="angkatan" placeholder="Angkatan" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Nama Pengguna</label>
                    <input type="text" class="form-control" value="<?php echo $row->nama_pengguna?>" name="nama_pengguna" placeholder="Nama Pengguna" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Kata sandi :</label>
                    <input type="password" class="form-control" data-minlength="6" name="kata_sandi" id="inputPassword<?php echo $i?>" placeholder="Kata sandi" required>
                    <span class="help-block">Minimal 6 karakter</span>
                    <div style="margin-top:10px;"></div>
                    <input type="password" class="form-control" name="rekata_sandi" data-match="#inputPassword<?php echo $i?>" data-match-error="Whoops, sandi tidak cocok" placeholder="Ulangi kata sandi" required>
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <label>No HP</label>
                    <input type="number" class="form-control" value="<?php echo $row->no_hp?>" name="no_hp" placeholder="No HP" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Level</label>
                    <select class="form-control" name="level" <?php if($row->level == "Super Admin" && $row->nif == $nif){ echo "disabled='disabled'"; }?>>
                      <option value="Super Admin" <?php if($row->level == "Super Admin"){ echo "selected"; }?> >Super Admin</option>
                      <option value="Admin" <?php if($row->level == "Admin"){ echo "selected"; }?> >Admin</option>
                    </select>
                  </div><!-- /.form-group -->
                  <?php
                    if($row->level == "Super Admin" && $row->nif == $nif){
                  ?>
                    <input type="hidden" name="level" value="Super Admin">
                  <?php    
                    }
                  ?>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                  </div>         
                </form>
              </div>          
            </div>
          </div>
        </div>

        <div class="modal fade" id="myFoto<?php echo $row->nif?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Foto <?php echo $row->nama;?></h4>
              </div>
              <div class="modal-body">
                <center>
                  <img src="<?=base_url()?>assets/img/fotoAdmin/<?php echo $row->foto;?>" class="img-circle" style="height:35%;width:35%;">
                </center>
              </div> 
              <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
              </div>
            </div>          
          </div>
        </div>


        <div class="modal fade" id="myUbahFoto<?php echo $row->nif?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ubah Foto <?php echo $row->nama;?></h4>
              </div>
              <div class="modal-body">
                <center>
                  <img src="<?=base_url()?>assets/img/fotoAdmin/<?php echo $row->foto;?>" class="img-circle" style="height:35%;width:35%;">
                </center>
                <br><br>
                <?php echo form_open_multipart('CSuperAdmin/UbahFoto');?>
                  <input type="hidden" name="nif" value="<?php echo $row->nif;?>"/>
                  <input type="hidden" name="nama" value="<?php echo $row->nama;?>"/>
                  <input type="hidden" name="foto" value="<?php echo $row->foto;?>"/>
                  <input type="file" name="userfile" size="20" class="form-control" required/>
                  <span class="help-block">Maks dimensi 160 x 160. Tipe file jpeg | jpg | png</span>
                  <br>
                  <button type="submit" class="btn btn-primary">Simpan perubahan</button>
                <?php echo form_close();?>
              </div> 
              <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
              </div>
            </div>          
          </div>
        </div>

        <div class="modal fade" id="hapusAdmin<?php echo $row->nif?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Hapus data admin</h4>
              </div>
              <div class="modal-body">
                  Apakah anda ingin menghapus data <?php echo $row->nama ?> ?
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                  <a href="<?=base_url()?>CSuperAdmin/hapusAdmin/<?php echo $row->nif; ?>/<?php echo $row->foto; ?>"><button type="button" id="confirmHapus" class="simpan btn btn-primary">Ya</button></a>
               </div>
            </div>          
          </div>
        </div>
      <?php
        }
      ?>

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; Komputer dan Sistem Informasi 2013.</strong>
      </footer>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url()?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?=base_url()?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?=base_url()?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url()?>assets/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url()?>assets/dist/js/demo.js"></script>
    <!-- page script -->
    <script src="<?=base_url()?>assets/js/validator.js"></script>
    <script type="text/javascript">
        function myFunction() {
            var nifInput = document.getElementById("nif").value;
              jQuery.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>" + "CSuperAdmin/cari_data_nif_ajax",
              dataType: 'json',
              data: {nif: nifInput},
              success: function(res) {
                if (res){
                  // Show Entered Value
                  //jQuery("div#result").show();
                  //jQuery("div#value").html(res.username);
                  //jQuery("div#value_pwd").html(res.pwd);
                  $("#nama").val(res.nama);
                  $("#nama1").val(res.nama);
                  $("#angkatan").val(res.angkatan);
                }
              }
            });
        }
    </script>
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
  </body>
</html>