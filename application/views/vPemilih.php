<!DOCTYPE html>
<html lang="en">
<head>
  <title>Pemira HIMAKOMSI</title>
  <link rel="icon" href="<?=base_url()?>/assets/img/HIMAKOMSI.jpg" type="image/gif">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/buttons2.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/demo.css">
  <script src="<?=base_url()?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    
    header { 
      background-color: #009f00;
      padding: 0px;
      width: 80%;
      height: 45px;
      margin: auto;
    }
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #009f00;
      font-size: 9px;
      padding: 0px;
      width: 80%;
      height: 10%;
      margin: auto;
    }

    #divcontent{
      width: 80%;
      height: 80%;
      margin: auto;
    }

    h1 {
      color: #fff;
      padding: 5px 5px;
      margin: auto;
      font-size: 28px;
      border-top: 1px solid rgba(255,255,255,0.5);
    }

    h2 {
      color: #000;
      padding: 10px -10px;
      margin: 0 30px;
      height: 0px;
      font-size: 20px;  
    }
    
    h3 {
      font-size: 20px;
      padding: 0px;
      }

    h4 {
      color: #fff;
      padding: 13px 5px;
      margin: 0;
      font-size: 15px;
      border-top: 1px solid rgba(255,255,255,0.5);
    }

    div#content1{ display:none;}
    div#content2{ display:none;}
    div#content3{ display:none;}

  .carousel-inner img {
      width: 150%; /* Set width to 100% */
      margin: auto;
      min-height:0px;
  }



  /* ini tambahan circle */
/*   .ch-img-4 { 
background-image: url(images/deny2.jpg);
}

.ch-img-5 { 
  background-image: url(images/deny2.jpg);
}

.ch-img-6 { 
  background-image: url(images/deny2.jpg);
} */

  .ch-item {
  width: 100%;
  height: 100%;
  border-radius: 50%;
  position: relative;
  cursor: default;
  box-shadow: 
    inset 0 0 0 0 rgba(24, 24, 24, 0.4),
    inset 0 0 0 16px rgba(255,255,255,0.6),
    0 1px 2px rgba(0,0,0,0.1);
    
  -webkit-transition: all 0.4s ease-in-out;
  -moz-transition: all 0.4s ease-in-out;
  -o-transition: all 0.4s ease-in-out;
  -ms-transition: all 0.4s ease-in-out;
  transition: all 0.4s ease-in-out;
}
.ch-info {
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  opacity: 0;
  
  -webkit-transition: all 0.4s ease-in-out;
  -moz-transition: all 0.4s ease-in-out;
  -o-transition: all 0.4s ease-in-out;
  -ms-transition: all 0.4s ease-in-out;
  transition: all 0.4s ease-in-out;
  
  -webkit-transform: scale(0);
  -moz-transform: scale(0);
  -o-transform: scale(0);
  -ms-transform: scale(0);
  transform: scale(0);
  
  -webkit-backface-visibility: hidden; /*for a smooth font */

}

.ch-info h3 {
  color: #fff;
  text-transform: uppercase;
  position: relative;
  letter-spacing: 2px;
  font-size: 22px;
  margin: 0 30px;
  padding: 65px 0 0 0;
  height: 110px;
  font-family: 'Open Sans', Arial, sans-serif;
  text-shadow: 
    0 0 1px #fff, 
    0 1px 2px rgba(0,0,0,0.3);
}

.ch-info p {
  color: #fff;
  padding: 10px 5px;
  font-style: italic;
  margin: 0 30px;
  font-size: 12px;
  border-top: 1px solid rgba(255,255,255,0.5);
}

.ch-info p a {
  display: block;
  color: #fff;
  color: rgba(255,255,255,0.7);
  font-style: normal;
  font-weight: 700;
  text-transform: uppercase;
  font-size: 9px;
  letter-spacing: 1px;
  padding-top: 4px;
  font-family: 'Open Sans', Arial, sans-serif;
}

.ch-info p a:hover {
  color: #fff222;
  color: rgba(24, 24, 24, 0.8);
}

.ch-item:hover {
  box-shadow: 
    inset 0 0 0 110px rgba(24, 24, 24, 0.4),
    inset 0 0 0 16px rgba(255,255,255,0.8),
    0 1px 2px rgba(0,0,0,0.1);
}

.ch-item:hover .ch-info {
  opacity: 1;
  
  -webkit-transform: scale(1);
  -moz-transform: scale(1);
  -o-transform: scale(1);
  -ms-transform: scale(1);
  transform: scale(1);  
}

.ch-grid {
  margin: 0 0 0 0;
  padding: 0;
  list-style: none;
  display: block;
  text-align: center;
  width: 100%;
}

.ch-grid:after,
.ch-item:before {
  content: '';
    display: table;
}

.ch-grid:after {
  clear: both;
}

.ch-grid li {
  width: 170px;
  height: 170px;
  display: inline-block;
  margin: 45px;
}

#back1 {
  background-image: url("<?=base_url()?>assets/img/bg.jpg");
  height: 100%;
  width: 100%;
}

/*div.bottom{background:#88A900; height:8%; width: 80%; margin: auto;}
p.CR{font-family: Clarendon Blk BT; font-size: 10px; text-align: center; padding-top: 11px; color: white;}
p.top{font-family: Clarendon Blk BT; color: white; font-size: 30px; text-align: center; padding-top: 10px;}
div.top{background:#88A900; height:10%; width: 80%; margin: auto;}
div.clear{clear: both;}*/

  </style>
</head>
<body id="back1">

<!-- <div class="top">
      <p class="top">PEMIRA HIMAKOMSI 2015</p>
    </div> -->
<header class="container-fluid text-center">
  <h1>Pemira HIMAKOMSI</h1>
</header>
<div id="divcontent" class="container text-center">
  <br>
  <p style="margin-left:80%;font-size:16px;"><b>Hello <?php echo $nama?> !</b></p>
  
  <h2>Calon Ketua HIMAKOMSI</h2>
  <div class="row">
  <section class="main">
        <ul class="ch-grid">
          <?php
            $i=0;
            foreach ($dataCalonKetua->result() as $value) {
            $i++;
          ?>
          <li>
            <div id="div<?php echo $i ?>" class="ch-item ch-img-4" style="background-image: url(<?=base_url()?>assets/img/fotoCalonKetua/<?php echo $value->foto ?>);">
              <div class="ch-info">
                <h3><?php echo $i?></h3>
                <p><?php echo $value->nama; ?></p>
              </div>
            </div>
          </li>
          <?php
            }
          ?>
        </ul>
        
      </section>
  </div>
    <?php 
      $i=0;
      foreach ($dataCalonKetua->result() as $row) {
      $i++;
    ?>
    <div id="content<?php echo $i;?>" class="col-md-9">
        <div class="visimisi">
          <p class="visimisi">
            <b>Nama</b><br>
            <?php echo $i.".".$row->nama;?><br>
            <b>Visi :</b><br>
            <?php echo $row->visi;?><br>
            <b>Misi</b> :<br>
            <?php echo $row->misi;?><br>
          </p>
          <center>
          <a href="<?=base_url() ?>CPemilih/vote/<?php echo $row->id_ketua?>/<?php echo $nif;?>" onclick="return confirm('Anda yakin vote ?')">
            <button type="button" class="button button--aylen button--border-thick button--inverted button--text-upper button--size-s" data-text="VOTE"><span>VOTE</span></button></a>
          </center>
        </div>
    </div>
    <?php
      }
    ?>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
</div>

<!-- <div class="clear">
    </div>

<div class="bottom">
      <p class="CR">Copyright &#169; 2014 By <a href="http://about.me/kurniawanhendiwijaya">Kurniawan Hendi Wijaya</a><br>Komputer dan Sistem Informasi 2013</p>
    </div> -->


<footer class="container-fluid text-center">
  <h4>Copyright &#169; Pemira Himakomsi design and build by Kelompok Proyek SI 2013</h4>
</footer>

</body>
<script>
$("#div1").click(function(){
    $("#content2").animate({left: '25%'}, "slow");
    $("#content2").hide();
    $("#content3").animate({left: '25%'}, "slow");
    $("#content3").hide();

    $("#content1").show();
    $("#content1").animate({left: '12.5%'}, "slow");
});

$("#div2").click(function(){
    $("#content1").animate({left: '25%'}, "slow");
    $("#content1").hide();
    $("#content3").animate({left: '25%'}, "slow");
    $("#content3").hide();
    
    $("#content2").show();
    $("#content2").animate({left: '12.5%'}, "slow");
});
$("#div3").click(function(){
    $("#content1").animate({left: '25%'}, "slow");
    $("#content1").hide();
    $("#content2").animate({left: '25%'}, "slow");
    $("#content2").hide();

    $("#content3").show();
    $("#content3").animate({left: '12.5%'}, "slow");
});

</script>


</html>
