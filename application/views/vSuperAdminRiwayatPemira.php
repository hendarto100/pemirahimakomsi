<?php $this->load->view('headerSuperAdmin');?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/img/fotoAdmin/<?php echo $foto?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $nama ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menu Navigasi</li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin">
                <i class="fa fa-hand-pointer-o"></i>
                <span>Hak Akses</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-list-alt"></i> <span>Data Pemilih</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih"><i class="fa fa-align-justify"></i>Semua data</a></li>
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih_belum_teregistrasi"><i class="fa fa-user-times"></i>Belum Teregistrasi</a></li>
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih_teregistrasi"><i class="fa fa-user-plus"></i>Teregistrasi</a></li>
              </ul>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_admin">
                <i class="fa fa-list"></i>
                <span>Data Admin</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_calon_ketua">
                <i class="fa fa-users"></i>
                <span>Data Calon Ketua</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_pemira">
                <i class="fa fa-calendar-minus-o"></i>
                <span>Data Pemira</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_perolehan_suara">
                <i class="fa fa-bar-chart"></i>
                <span>Perolehan Suara</span>
              </a>
            </li>
            <li class="active">
              <a href="<?=base_url()?>CSuperAdmin/hal_riwayat_pemira">
                <i class="fa fa-line-chart"></i>
                <span>Riwayat Pemira</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" >
          <h1>
            Riwayat Pemira tahun <?php echo $tahun?>
          </h1>
          <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Perolehan Suara</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="form-group" style="margin-left:1.5%;">
              <label>Pilih tahun pemira : </label>
              <form action="<?=base_url()?>CSuperAdmin/hal_riwayat_pemira" method="POST">
                <div class="input-group input-group-sm" style="width:20%;">
                  <select name="tahun" id="tahun" class="form-control">
                    <?php
                      foreach ($tahunPemira->result() as $value) {
                    ?>
                      <option value="<?php echo $value->tahun?>" <?php if($value->tahun == $tahun){echo "selected";} ?>><?php echo $value->tahun?></option>
                    <?php
                      }
                    ?>
                  </select>
                  <span class="input-group-btn">
                    <button class="btn btn-info btn-flat" type="submit">Tampil</button>
                  </span>
                </div>
              </form>
            </div><!-- /.form-group -->
            <br>
            <div class="col-md-6">
              <!-- DONUT CHART -->
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Perolehan Suara</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <?php if($perolehanSuara->result() == null){
                      echo "Belum ada suara masuk";}
                  ?>
                    <canvas id="pieChart" style="height:250px"></canvas>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col (LEFT) -->
            <div class="col-md-6">
              <!-- DONUT CHART -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Status Pilih</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <?php if($statusPemilih->result() == null){
                      echo "Belum ada pemilih yang melakukan vote";}
                  ?>
                    <canvas id="pieChart1" style="height:250px"></canvas>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col (RIGHT) -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; Komputer dan Sistem Informasi 2013.</strong>
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url()?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="<?=base_url()?>assets/plugins/chartjs/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="<?=base_url()?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url()?>assets/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url()?>assets/dist/js/demo.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        /* ChartJS
         * -------
         * Here we will create a few charts using ChartJS
         */
        //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
        var pieChart = new Chart(pieChartCanvas);
        var PieData = [
        <?php
          $color1 = "#00c0ef";
          $highlight1 = "#06D0FF";
          $color2 = "#F56A54";
          $highlight2 = "#FF8E7C";
          $color3 = "#F39C12";
          $highlight3 = "#FFB848";
          $i=0;
          foreach ($perolehanSuara->result() as $value) {
            $i++;
        ?>
          {
            value: <?php echo $value->poling ?>,
            color: "<?php if($i==1){
                            echo $color1;
                          }else if($i==2){
                            echo $color2;
                          }else{
                            echo $color3;
                          }
                    ?>",
            highlight: "<?php if($i==1){
                            echo $highlight1;
                          }else if($i==2){
                            echo $highlight2;
                          }else{
                            echo $highlight3;
                          }
                        ?>",
            label: "<?php echo $value->nama ?>"
          },
        <?php
          }
        ?>
        ];
        var pieOptions = {
          //Boolean - Whether we should show a stroke on each segment
          segmentShowStroke: true,
          //String - The colour of each segment stroke
          segmentStrokeColor: "#fff",
          //Number - The width of each segment stroke
          segmentStrokeWidth: 2,
          //Number - The percentage of the chart that we cut out of the middle
          percentageInnerCutout: 50, // This is 0 for Pie charts
          //Number - Amount of animation steps
          animationSteps: 100,
          //String - Animation easing effect
          animationEasing: "easeOutBounce",
          //Boolean - Whether we animate the rotation of the Doughnut
          animateRotate: true,
          //Boolean - Whether we animate scaling the Doughnut from the centre
          animateScale: false,
          //Boolean - whether to make the chart responsive to window resizing
          responsive: true,
          // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio: true,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(PieData, pieOptions);


        //-------------
        //- PIE CHART1-
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var pieChart1Canvas = $("#pieChart1").get(0).getContext("2d");
        var pieChart1 = new Chart(pieChart1Canvas);
        var PieData = [
        <?php
          $color1 = "#00A65B";
          $highlight1 = "#27BB78";
          $color2 = "#D2D6DE";
          $highlight2 = "#FAFAFB";
          $i=0;
          foreach ($statusPemilih->result() as $row) {
            $i++;
        ?>
          {
            value: <?php echo $row->jumlah ?>,
            color: "<?php if($i==1){
                            echo $color1;
                          }else if($i==2){
                            echo $color2;
                          }else{
                            echo $color3;
                          }
                    ?>",
            highlight: "<?php if($i==1){
                            echo $highlight1;
                          }else if($i==2){
                            echo $highlight2;
                          }else{
                            echo $highlight3;
                          }
                        ?>",
            label: "<?php echo $row->status_pilih ?>"
          },
        <?php
          }
        ?>
        ];
        var pieOptions = {
          //Boolean - Whether we should show a stroke on each segment
          segmentShowStroke: true,
          //String - The colour of each segment stroke
          segmentStrokeColor: "#fff",
          //Number - The width of each segment stroke
          segmentStrokeWidth: 2,
          //Number - The percentage of the chart that we cut out of the middle
          percentageInnerCutout: 50, // This is 0 for Pie charts
          //Number - Amount of animation steps
          animationSteps: 100,
          //String - Animation easing effect
          animationEasing: "easeOutBounce",
          //Boolean - Whether we animate the rotation of the Doughnut
          animateRotate: true,
          //Boolean - Whether we animate scaling the Doughnut from the centre
          animateScale: false,
          //Boolean - whether to make the chart responsive to window resizing
          responsive: true,
          // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio: true,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChart1.Doughnut(PieData, pieOptions);

      });
    </script>
  </body>
</html>
