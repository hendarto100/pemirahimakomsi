<?php $this->load->view('headerAdmin');?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/img/fotoAdmin/<?php echo $foto?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $nama ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menu Navigasi</li>
            <li class="active">  
              <a href="<?=base_url()?>CAdmin">
                <i class="fa fa-hand-pointer-o"></i>
                <span>Hak Akses</span>
              </a> 
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-list-alt"></i> <span>Data Pemilih</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>CAdmin/hal_data_pemilih"><i class="fa fa-align-justify"></i>Semua data</a></li>
                <li><a href="<?=base_url()?>CAdmin/hal_data_pemilih_belum_teregistrasi"><i class="fa fa-user-times"></i>Belum Teregistrasi</a></li>
                <li><a href="<?=base_url()?>CAdmin/hal_data_pemilih_teregistrasi"><i class="fa fa-user-plus"></i>Teregistrasi</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Hak Akses Pemilih
            <small>Registrasi</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Hak Akses</li>
          </ol>
        </section>
        
        <section class="content-header">
          <div>
            <?php 
              if($this->session->flashdata('sukses')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Berhasil !</strong>Pemberian hak akses kepada <b><?php echo $namaPemilih;?></b>
                </div>
            <?php
              }else if($this->session->flashdata('gagalTanggal')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Gagal registrasi !</strong>Bukan dalam periode pemira
                </div>
            <?php
              }else if($this->session->flashdata('gagal')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Gagal registrasi ! </strong>Pemilih sudah ter-registrasi atau sudah bukan mahasiswa aktif 
                </div>
            <?php
              }
            ?>
          </div>

        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="small-box bg-green" style="width:50%; margin:auto; margin-top:5%;">
                <a href="" class="small-box-footer" style="font-size:16px;">Form Registrasi Hak Akses<br>Pemira HIMAKOMSI</a>
                <div class="inner">
                  <b style="font-size:15px;">Masukan NIF pemilih</b><br><br>
                  <form action="<?=base_url()?>CAdmin/registrasiHakAkses" method="post">
                    <?php foreach ($pemiraTahunSekarang->result() as $value) {?>
                      <input type="hidden" name="tanggal_mulai" value="<?php echo $value->tanggal_mulai;?>">
                      <input type="hidden" name="tanggal_berakhir" value="<?php echo $value->tanggal_berakhir;?>">
                      <input type="hidden" name="id_pemira" value="<?php echo $value->id_pemira;?>">
                    <?php }?>
                    <input type="number" name="nif" placeholder="NIF (Nomor Induk Fakultas)" class="form-control" required autofocus>
                    <br>
                    <input type="submit" value="Kirim" class="btn btn-default addButton">
                  </form>
                </div>
                <div class="icon" >
                  <i class="fa fa-registered"></i>
                </div>
                <a href="" class="small-box-footer">Himpunan Mahasiswa Komputer dan Sistem Informasi<br>Universitas Gadjah Mada</a>
              </div>
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $this->load->view('footerAdmin');?>