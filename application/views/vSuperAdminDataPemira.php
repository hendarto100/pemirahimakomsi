<?php $this->load->view('headerSuperAdmin');?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/img/fotoAdmin/<?php echo $foto?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $nama ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menu Navigasi</li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin">
                <i class="fa fa-hand-pointer-o"></i>
                <span>Hak Akses</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-list-alt"></i> <span>Data Pemilih</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih"><i class="fa fa-align-justify"></i>Semua data</a></li>
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih_belum_teregistrasi"><i class="fa fa-user-times"></i>Belum Teregistrasi</a></li>
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih_teregistrasi"><i class="fa fa-user-plus"></i>Teregistrasi</a></li>
              </ul>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_admin">
                <i class="fa fa-list"></i>
                <span>Data Admin</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_calon_ketua">
                <i class="fa fa-users"></i>
                <span>Data Calon Ketua</span>
              </a>
            </li>
            <li class="active">
              <a href="<?=base_url()?>CSuperAdmin/hal_data_pemira">
                <i class="fa fa-calendar-minus-o"></i>
                <span>Data Pemira</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_perolehan_suara">
                <i class="fa fa-bar-chart"></i>
                <span>Perolehan Suara</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_riwayat_pemira">
                <i class="fa fa-line-chart"></i>
                <span>Riwayat Pemira</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Pemira HIMAKOMSI
          </h1>
          <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Data Pemira</li>
          </ol>
        </section>
        
        <section class="content-header">
          <div>
            <?php 
              if($this->session->flashdata('berhasil')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Ubah data <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('gagalUbah')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Ubah data <strong>gagal !</strong> Dalam periode pemira atau lebih
                </div>
            <?php
              }else if($this->session->flashdata('gagalTambahKandidat')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Tambah kandidat <strong>gagal !</strong> Dalam periode pemira atau lebih
                </div>
            <?php
              }else if($this->session->flashdata('suksestambahkandidat')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Tambah kandidat <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('suksestambah')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Tambah ada pemira <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('gagaltambah')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('pesanGagal'); ?>
                </div>
            <?php
              }else if($this->session->flashdata('uploadFotoGagal')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $error;?>
                </div>
            <?php
              }else if($this->session->flashdata('ubahFoto')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Ubah <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('berhasilHapusPemira')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Hapus data <strong>berhasil !</strong>
                </div>
            <?php
              }
            ?>
          </div>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Mulai</th>
                        <th>Tanggal Berakhir</th>
                        <th>Nama Kandidat</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                    	<?php
                    		$i=0;
                    		foreach ($pemira->result() as $row) {
                          $i++;
                    	?>
	                      <tr>
	                        <td><?php echo $i;?></td>
	                        <td><?php echo $row->tanggal_mulai;?></td>
	                        <td><?php echo $row->tanggal_berakhir;?></td>
                          <td>
                            <?php
                              foreach ($kandidatPemiraTerdaftar->result() as $value) {
                                if($row->id_pemira == $value->id_pemira){
                                  echo $value->nama_kandidat;
                                }
                              }
                            ?>
	                        <td>
                            <?php if(strtotime($row->tanggal_mulai) > strtotime(date("Y-m-d"))){ ?>
                              <a type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal<?php echo $row->id_pemira?>"><i class="glyphicon glyphicon-pencil"></i>&nbsp; Ubah</a>
	                          <?php } ?>
                          </td>
	                      </tr>
	                    <?php
	                    	}
	                    ?>
                  </table>
                  <a type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah"><i class="glyphicon glyphicon-plus"></i> Tambah Pemira</a>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <!-- /.modalTambah -->
        <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Data Pemira</h4>
              </div>
              <div class="modal-body">
                <?php 
                  $attributes = array('data-toggle'=>'validator');
                  echo form_open_multipart('CSuperAdmin/tambahDataPemira',$attributes); 
                ?>
                  <input type="hidden" value="<?php echo $id_pemira?>" name="id_pemira"?>
                  <div class="form-group">
                    <label>Id Pemira</label>
                    <input type="text" class="form-control" name="" value="<?php echo $id_pemira;?>" placeholder="NIF" required disabled>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Tanggal Mulai</label>
                    <input type="date" class="form-control" name="tanggal_mulai" placeholder="Tanggal Mulai" required autofocus>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Tanggal Berakhir</label>
                    <input type="date" class="form-control" name="tanggal_berakhir" placeholder="Tanggal Berakhir" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Calon Kandidat 1</label>
                    <select name="calon_kandidat_1" class="form-control" required>
                      <?php
                        foreach ($calon_kandidat->result() as $value) {
                      ?>
                        <option value="<?php echo $value->id_ketua;?>">
                          <?php echo $value->nama ;?>
                        </option>
                      <?php
                        }
                      ?>
                    </select>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Calon Kandidat 2</label>
                    <select name="calon_kandidat_2" class="form-control" required>
                      <?php
                        foreach ($calon_kandidat->result() as $value) {
                      ?>
                        <option value="<?php echo $value->id_ketua;?>">
                          <?php echo $value->nama ;?>
                        </option>
                      <?php
                        }
                      ?>
                    </select>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Calon Kandidat 3</label>
                    <select name="calon_kandidat_3" class="form-control" required>
                      <?php
                        foreach ($calon_kandidat->result() as $value) {
                      ?>
                        <option value="<?php echo $value->id_ketua;?>">
                          <?php echo $value->nama ;?>
                        </option>
                      <?php
                        }
                      ?>
                    </select>
                  </div><!-- /.form-group -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                  </div>         
                <!-- </form> -->
                <?php echo form_close();?>
              </div>          
            </div>
          </div>
        </div>

      <!-- modal ubah data -->
      <?php
        $i=0;
        foreach ($pemira->result() as $row) {
        $i++;
      ?>
        
        <div class="modal fade" id="myModal<?php echo $row->id_pemira?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ubah Data Pemira</h4>
              </div>
              <div class="modal-body">
                <form method="post" action="<?=base_url()?>CSuperAdmin/ubahDataPemira" data-toggle="validator">
                  <input type="hidden" name="id_pemira" value="<?php echo $row->id_pemira;?>">
                  <input type="hidden" name="tanggal_mulai_awal" value="<?php echo $row->tanggal_mulai;?>">
                  <div class="form-group">
                    <label>ID Pemira</label>
                    <input type="text" class="form-control" value="<?php echo $row->id_pemira?>" name="" placeholder="id_pemira" required disabled>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Tanggal Mulai</label>
                    <input type="date" class="form-control" name="tanggal_mulai" value="<?php echo $row->tanggal_mulai; ?>" placeholder="Tanggal Mulai" required autofocus>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Tanggal Berakhir</label>
                    <input type="date" class="form-control" name="tanggal_berakhir" value="<?php echo $row->tanggal_berakhir; ?>" placeholder="Tanggal Berakhir" required>
                  </div><!-- /.form-group -->  
                  <?php
                  $j = 0;
                    foreach ($kandidatPemira->result() as $value) {
                      if($row->id_pemira == $value->id_pemira){
                         $j++;
                  ?>
                    <input type="hidden" name="calon_kandidat_<?php echo $j ?>_awal" value="<?php echo $value->id_ketua;?>">
                    <div class="form-group">
                      <label>Calon Kandidat <?php echo $j;?></label>
                      <select name="calon_kandidat_<?php echo $j;?>" class="form-control" required>
                        <option value="<?php echo $value->id_ketua;?>">
                          <?php echo $value->nama ;?>
                        </option>
                        <?php
                          foreach ($calon_kandidat->result() as $isi) {
                        ?>
                          <option value="<?php echo $isi->id_ketua;?>">
                            <?php echo $isi->nama ;?>
                          </option>
                        <?php
                          }
                        ?>
                      </select>
                    </div><!-- /.form-group -->
                  <?php
                      }
                    }
                    if($j<3){
                  ?>
                    <a href="" data-toggle="modal" data-target="#tambahKandidat<?php echo $row->id_pemira;?>">Tambah Kandidat</a>
                  <?php    
                    }
                  ?>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                  </div>         
                </form>
              </div>          
            </div>
          </div>
        </div>

        <!--Tambah Kandidat-->
        <div class="modal fade" id="tambahKandidat<?php echo $row->id_pemira?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Kandidat</h4>
              </div>
              <div class="modal-body">
                <form method="post" action="<?=base_url()?>CSuperAdmin/tambahKandidat" data-toggle="validator">
                  <input type="hidden" name="id_pemira" value="<?php echo $row->id_pemira;?>">
                  <input type="hidden" name="tanggal_mulai" value="<?php echo $row->tanggal_mulai;?>">
                  <div class="form-group">
                    <label>ID Pemira</label>
                    <input type="text" class="form-control" value="<?php echo $row->id_pemira?>" name="" placeholder="id_pemira" required disabled>
                  </div><!-- /.form-group -->
                  <?php
                    do{
                      $j++;
                  ?>
                      <div class="form-group">
                      <label>Calon Kandidat <?php echo $j ?></label>
                      <select name="calon_kandidat_<?php echo $j ?>" class="form-control" required>
                        <?php
                          foreach ($calon_kandidat->result() as $value) {
                        ?>
                          <option value="<?php echo $value->id_ketua;?>">
                            <?php echo $value->nama ;?>
                          </option>
                        <?php
                          }
                        ?>
                      </select>
                    </div><!-- form-group -->
                  <?php
                    }while ($j<3);
                  ?>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                  </div>         
                </form>
              </div>          
            </div>
          </div>
        </div>
      <?php
        }
      ?>

      <!-- modal ubah foto -->
      <?php
        /*foreach ($calonKetua->result() as $row) {*/
      ?>
        <!-- <div class="modal fade" id="myUbahFoto<?php echo $row->id_ketua?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ubah Foto <?php echo $row->nama;?></h4>
              </div>
              <div class="modal-body">
                <center>
                  <img src="<?=base_url()?>assets/img/fotoCalonKetua/<?php echo $row->foto;?>" class="img-circle" style="height:35%;width:35%;">
                </center>
                <br><br>
                <?php echo form_open_multipart('CSuperAdmin/UbahFotoCalonKetua');?>
                  <input type="hidden" name="id_ketua" value="<?php echo $row->id_ketua;?>"/>
                  <input type="hidden" name="nama" value="<?php echo $row->nama;?>"/>
                  <input type="hidden" name="foto" value="<?php echo $row->foto;?>"/>
                  <input type="file" name="userfile" size="20" class="form-control" required/>
                  <span class="help-block">Maks dimensi 160 x 160. Tipe file jpeg | jpg | png</span>
                  <br>
                  <button type="submit" class="btn btn-primary">Simpan perubahan</button>
                <?php echo form_close();?>
              </div> 
              <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
              </div>
            </div>          
          </div>
        </div> -->
      <?php
       /* }*/
      ?>













      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; Komputer dan Sistem Informasi 2013.</strong>
      </footer>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url()?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?=base_url()?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?=base_url()?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url()?>assets/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url()?>assets/dist/js/demo.js"></script>
    <!-- page script -->
    <script src="<?=base_url()?>assets/js/validator.js"></script>
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
  </body>
</html>