<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="UTF-8" />  
        <title>Login Pemilih Pemira</title>
        <link rel="icon" href="<?=base_url()?>/assets/img/HIMAKOMSI.jpg" type="image/gif">
        <meta name="keywords" content="css3, login, form, custom, input, submit, button, html5, placeholder" />
        <style>
			body {
				background-image: url("<?=base_url()?>assets/img/bg.jpg");
			}


            /* GLOBALS */

            *,
            *:after,
            *:before {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                -ms-box-sizing: border-box;
                -o-box-sizing: border-box;
                box-sizing: border-box;
                padding: 0;
                margin: 0;
            }

            .clearfix:after {
                content: "";
                display: table;
                clear: both;
            }


            			/* Demo 2 */

            .form-2 {
                /* Size and position */
                width: 380px;
                margin: 60px auto 50px;
                padding: 25px;
                position: relative;

                /* Styles */
               	background-image: url("<?=base_url()?>assets/img/bg.jpg");
                border-radius: 10px;
                color: #f0f0f0;
                box-shadow:
                    0 40px 40px rgba(0,0,0,0.2),        
                    0 90px 90px rgba(0,0,0,0.2),        
                    0 0 0 12px rgb(0, 159, 0); 
            }

            h2 {
                  background-color: #228b22;
                  padding: 10px -10px;
                  margin: 0 30px;
                  height: 0px;
                  font-size: 25px;
                }

            #imghima {
              padding: 30px -30px;
              margin: 0 30px;
              height: 50px;
              font-size: 25px;  
            }


            .form-2 h1 {
                font-size: 15px;
                font-weight: bold;
                color: #bdb5aa;
                padding-bottom: 8px;
                border-bottom: 1px solid #EBE6E2;
                number-shadow: 0 2px 0 rgba(255,255,255,0.8);
                box-shadow: 0 1px 0 rgba(255,255,255,0.8);
            }

            .form-2 h1 .log-in{
                display: inline-block;
                number-transform: uppercase;
            }

            .form-2 h1 .log-in {
                color: #6c6763;
                padding-right: 2px;
            }

            .form-2 .float {
                width: 100%;
                float: left;
                padding-top: 15px;
                border-top: 1px solid rgb(0, 159, 0);
            }

            .form-2 label {
                display: block;
                padding: 0 0 5px 2px;
                cursor: pointer;
                number-transform: uppercase;
                font-weight: 400;
                number-shadow: 0 1px 0 rgba(255,255,255,0.8);
                font-size: 11px;
                color: #000;
            }


            .form-2 input[type=number] {
                font-family: 'Lato', Calibri, Arial, sans-serif;
                font-size: 13px;
                font-weight: 400;
                display: block;
                width: 100%;
                padding: 10px;
                margin-bottom: 15px;
                border: 3px solid #ebe6e2;
                border-radius: 5px;
                color: #000;
                -webkit-transition: all 0.3s ease-out;
                -moz-transition: all 0.3s ease-out;
                -ms-transition: all 0.3s ease-out;
                -o-transition: all 0.3s ease-out;
                transition: all 0.3s ease-out;
            }

            .form-2 input[type=number]:hover {
                border-color: #CCC;
            }

            .form-2 label:hover ~ input {
                border-color: #CCC;
            }

            .form-2 input[type=number]:focus {
                border-color: #BBB;
                outline: none; /* Remove Chrome's outline */
            }

            .form-2 input[type=submit]{
                /* Size and position */
                width: 29%;
                height: 28px;
                float: left;
                position: relative;

                /* Styles */
                box-shadow: inset 0 1px rgba(255,255,255,0.3);
                border-radius: 3px;
                cursor: pointer;

                /* Font styles */
                font-family: 'Lato', Calibri, Arial, sans-serif;
                font-size: 12px;
                line-height: 25px; /* Same as height */
                number-align: center;
                font-weight: bold;
            }

            .form-2 input[type=submit] {
                margin-left: 71%;
                background: #228b22; /* Fallback */
                border: 1px solid #000;
                color: #ffffff;
                number-shadow: 0 1px rgba(255,255,255,0.3);
            }

            .notif{
                width:28%;
                background:#dd4c39; 
                margin:auto; 
                margin-top:3%; 
                margin-bottom:-2%; 
                padding-top:0.5%; 
                padding-bottom:0.5%;
                border-radius: 10px; 
                text-align: center; 
                color:white;
            }
            .notifVote{
                width:28%;
                background:#00a65a; 
                margin:auto; 
                margin-top:3%; 
                margin-bottom:-2%; 
                padding-top:0.5%; 
                padding-bottom:0.5%;
                border-radius: 10px; 
                text-align: center; 
                color:white;
            }
		</style>
        <script>
            function notif() {
                document.getElementById("notif").style.display = "none";
            }
        </script>
    </head>
    <body>
        <div class="container">
			
			<header>
			</header>
			<br>
			<br>
			<br>
			<br>
            <center>
            <h2>
                <b>PEMIRA HIMAKOMSI</b>
                <img id="imghima" src="<?=base_url()?>assets/img/logohimakomsi.jpg"/>
            </h2>
            </center>
            <br>
            <br>
            <?php 
              if($this->session->flashdata('gagal')){
            ?>
                <div class="notif" id="notif">
                  <a href="" onclick="notif();" style="float:right; margin-right:3%;">&times;</a>
                  Masuk <strong>gagal !</strong> <br>Data belum diregistrasi atau bukan mahasiswa aktif.
                </div>
            <?php
              }else if($this->session->flashdata('gagal1')){
            ?>
                <div class="notif" id="notif">
                  <a href="" onclick="notif();" style="float:right; margin-right:3%;">&times;</a>
                  Masuk <strong>gagal !</strong> <br>Bukan periode pemira.
                </div>
            <?php
              }else if($this->session->flashdata('vote')){
            ?>
                <div class="notifVote" id="notif">
                  <a href="" onclick="notif();" style="float:right; margin-right:3%;">&times;</a>
                  Selamat vote <strong>Berhasil !</strong> <br>
                </div>
            <?php
              }
            ?>
			<section class="main">
                <?php foreach ($pemiraTahunSekarang->result() as $value) { ?>
				<form class="form-2" action="CPemilih/cek_login" method="post">
                    <input type="hidden" value="<?php echo $value->tanggal_mulai?>" name="tanggal_muali">
                    <input type="hidden" value="<?php echo $value->tanggal_berakhir?>" name="tanggal_berakhir">
					<center>
					<h1><span class="log-in">Login Pemira Himakomsi</span></h1>
					</center>
					<p class="float">
						<label for="login">Masukkan NIF</label>
						<input type="number" name="nif" placeholder="NIF (Nomor Induk Fakultas)" autofocus required>
					</p>
					<p class="clearfix">     
						 <input type="submit" name="submit" value="Masuk">
					</p>
				</form>
                <?php
                    }
                ?>​​
			</section>
			
        </div>
		
    </body>
</html>