<?php $this->load->view('headerSuperAdmin');?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/img/fotoAdmin/<?php echo $foto?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $nama ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menu Navigasi</li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin">
                <i class="fa fa-hand-pointer-o"></i>
                <span>Hak Akses</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-list-alt"></i> <span>Data Pemilih</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih"><i class="fa fa-align-justify"></i>Semua data</a></li>
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih_belum_teregistrasi"><i class="fa fa-user-times"></i>Belum Teregistrasi</a></li>
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih_teregistrasi"><i class="fa fa-user-plus"></i>Teregistrasi</a></li>
              </ul>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_admin">
                <i class="fa fa-list"></i>
                <span>Data Admin</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_calon_ketua">
                <i class="fa fa-users"></i>
                <span>Data Calon Ketua</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_pemira">
                <i class="fa fa-calendar-minus-o"></i>
                <span>Data Pemira</span>
              </a>
            </li>
            <li class="active">
              <a href="<?=base_url()?>CSuperAdmin/hal_perolehan_suara">
                <i class="fa fa-bar-chart"></i>
                <span>Perolehan Suara</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_riwayat_pemira">
                <i class="fa fa-line-chart"></i>
                <span>Riwayat Pemira</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Perolehan Suara Pemira HIMAKOMSI
          </h1>
          <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Perolehan Suara</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Perolehan Suara tahun <?php echo $tahun?></h3>
                </div>
                <div class="box-body">
                  <div class="form-group">
                    <label>Pilih tahun pemira : </label>
                    <form action="<?=base_url()?>CSuperAdmin/hal_perolehan_suara" method="POST">
                      <div class="input-group input-group-sm" style="width:20%;">
                        <select name="tahun" id="tahun" class="form-control">
                        <?php
                          foreach ($tahunPemira->result() as $value) {
                        ?>
                          <option value="<?php echo $value->tahun?>" <?php if($value->tahun == $tahun){echo "selected";} ?>><?php echo $value->tahun?></option>
                        <?php
                          }
                        ?>
                      </select>
                        <span class="input-group-btn">
                          <button class="btn btn-info btn-flat" type="submit">Tampil</button>
                        </span>
                      </div>
                    </form>
                    
                  </div><!-- /.form-group -->
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>NIF</th>
                        <th>Nama</th>
                        <th>Poling</th>
                      </tr>
                    </thead>
                    <tbody>
                    	<?php
                    		$i=0;
                    		foreach ($perolehanSuara->result() as $row) {
                          $i++;
                    	?>
	                      <tr>
	                        <td><?php echo $i;?></td>
	                        <td><?php echo $row->id_ketua;?></td>
	                        <td><?php echo $row->nama;?></td>
	                        <td><?php echo $row->poling;?></td>
	                      </tr>
	                    <?php
	                    	}
	                    ?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->



      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; Komputer dan Sistem Informasi 2013.</strong>
      </footer>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url()?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?=base_url()?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?=base_url()?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url()?>assets/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url()?>assets/dist/js/demo.js"></script>
    <!-- page script -->
    <script src="<?=base_url()?>assets/js/validator.js"></script>
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
  </body>
</html>