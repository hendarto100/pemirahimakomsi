<?php $this->load->view('headerSuperAdmin');?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/img/fotoAdmin/<?php echo $foto?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $nama ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menu Navigasi</li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin">
                <i class="fa fa-hand-pointer-o"></i>
                <span>Hak Akses</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-list-alt"></i> <span>Data Pemilih</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih"><i class="fa fa-align-justify"></i>Semua data</a></li>
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih_belum_teregistrasi"><i class="fa fa-user-times"></i>Belum Teregistrasi</a></li>
                <li><a href="<?=base_url()?>CSuperAdmin/hal_data_pemilih_teregistrasi"><i class="fa fa-user-plus"></i>Teregistrasi</a></li>
              </ul>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_admin">
                <i class="fa fa-list"></i>
                <span>Data Admin</span>
              </a>
            </li>
            <li class="active">
              <a href="<?=base_url()?>CSuperAdmin/hal_data_calon_ketua">
                <i class="fa fa-users"></i>
                <span>Data Calon Ketua</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_data_pemira">
                <i class="fa fa-calendar-minus-o"></i>
                <span>Data Pemira</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_perolehan_suara">
                <i class="fa fa-bar-chart"></i>
                <span>Perolehan Suara</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url()?>CSuperAdmin/hal_riwayat_pemira">
                <i class="fa fa-line-chart"></i>
                <span>Riwayat Pemira</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Calon Ketua Pemira HIMAKOMSI
          </h1>
          <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Data Calon Ketua</li>
          </ol>
        </section>
        
        <section class="content-header">
          <div>
            <?php 
              if($this->session->flashdata('berhasil')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Ubah data <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('nifAda')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Ubah data <strong>gagal !</strong> NIF sudah ada
                </div>
            <?php
              }else if($this->session->flashdata('suksestambah')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Tambah ada calon ketua <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('gagaltambah')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Tambah data calon ketua <strong>gagal !</strong> NIF sudah ada
                </div>
            <?php
              }else if($this->session->flashdata('uploadFotoGagal')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $error;?>
                </div>
            <?php
              }else if($this->session->flashdata('ubahFoto')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Ubah <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('berhasilHapusCalonKetua')){
            ?>
                <div class="alert alert-success fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Hapus data <strong>berhasil !</strong>
                </div>
            <?php
              }else if($this->session->flashdata('gagalHapusCalonKetua')){
            ?>
                <div class="alert alert-danger fade in">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Hapus data <strong>gagal !</strong> Pemira sudah berlangsung.
                </div>
            <?php
              }
            ?>
          </div>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>NIF</th>
                        <th>Nama</th>
                        <th>Angkatan</th>
                        <th>Visi</th>
                        <th>Misi</th>
                        <th>Foto</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                    	<?php
                    		$i=0;
                    		foreach ($calonKetua->result() as $row) {
                          $i++;
                    	?>
	                      <tr>
	                        <td><?php echo $i;?></td>
	                        <td><?php echo $row->id_ketua;?></td>
	                        <td><?php echo $row->nama;?></td>
	                        <td><?php echo $row->angkatan;?></td>
	                        <td><?php echo $row->visi;?></td>
                          <td><?php echo $row->misi;?></td>
                          <td>
                            <a data-toggle="modal" data-target="#myFoto<?php echo $row->id_ketua?>"><img src="<?=base_url()?>assets/img/fotoCalonKetua/<?php echo $row->foto;?>" class="img-circle" style="height:20%;"></a>
                            <a data-toggle="modal" data-target="#myUbahFoto<?php echo $row->id_ketua?>"> &nbsp;&nbsp;&nbsp;&nbsp;Ubah foto</a> 
                          </td>
	                        <td>
	                        	<a type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal<?php echo $row->id_ketua?>"><i class="glyphicon glyphicon-pencil"></i>&nbsp; Ubah</a>
                            <a type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapusKetua<?php echo $row->id_ketua?>"><i class="glyphicon glyphicon-trash"></i>&nbsp; Hapus</a>
	                        </td>
	                      </tr>
	                    <?php
	                    	}
	                    ?>
                  </table>
                  <a type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah"><i class="glyphicon glyphicon-plus"></i> Tambah Calon Ketua</a>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <!-- /.modalTambah -->
        <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Data Calon Ketua</h4>
              </div>
              <div class="modal-body">
                <?php 
                  $attributes = array('data-toggle'=>'validator');
                  echo form_open_multipart('CSuperAdmin/tambahDataCalonKetua',$attributes); 
                ?>
                  <div class="form-group">
                    <label>NIF</label>
                    <input type="number" class="form-control" name="nif" placeholder="NIF" required autofocus>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="nama" placeholder="Nama" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Angkatan</label>
                    <input type="number" class="form-control" name="angkatan" placeholder="Angkatan" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Visi</label>
                    <textarea class="form-control" placeholder="Visi" name="visi" required></textarea>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Misi</label>
                    <textarea class="form-control" placeholder="Misi" name="misi" required></textarea>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Foto</label>
                    <input type="file" name="userfile" size="20" class="form-control"/>
                    <span class="help-block">Maks dimensi 160 x 160. Tipe file jpeg | jpg | png</span>
                  </div><!-- /.form-group -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                  </div>         
                <!-- </form> -->
                <?php echo form_close();?>
              </div>          
            </div>
          </div>
        </div>

      <!-- modal ubah data -->
      <?php
        $i=0;
        foreach ($calonKetua->result() as $row) {
        $i++;
      ?>
        
        <div class="modal fade" id="myModal<?php echo $row->id_ketua?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ubah Data Calon Ketua</h4>
              </div>
              <div class="modal-body">
                <form method="post" action="<?=base_url()?>CSuperAdmin/ubahDataCalonKetua" data-toggle="validator">
                  <input type="hidden" name="nifAwal" value="<?php echo $row->id_ketua?>">
                  <div class="form-group">
                    <label>NIF</label>
                    <input type="number" class="form-control" value="<?php echo $row->id_ketua?>" name="nif" placeholder="NIF" required autofocus>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" value="<?php echo $row->nama?>" name="nama" placeholder="Nama" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Angkatan</label>
                    <input type="number" class="form-control" value="<?php echo $row->angkatan?>" name="angkatan" placeholder="Angkatan" required>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Visi</label>
                    <textarea class="form-control" placeholder="Visi" name="visi" required><?php echo $row->visi;?></textarea>
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Misi</label>
                    <textarea class="form-control" placeholder="Misi" name="misi" required><?php echo $row->misi;?></textarea>
                  </div><!-- /.form-group -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                  </div>         
                </form>
              </div>          
            </div>
          </div>
        </div>

        <div class="modal fade" id="myFoto<?php echo $row->id_ketua?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Foto <?php echo $row->nama;?></h4>
              </div>
              <div class="modal-body">
                <center>
                  <img src="<?=base_url()?>assets/img/fotoCalonKetua/<?php echo $row->foto;?>" class="img-circle" style="height:35%;width:35%;">
                </center>
              </div> 
              <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
              </div>
            </div>          
          </div>
        </div>

        <div class="modal fade" id="myUbahFoto<?php echo $row->id_ketua?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ubah Foto <?php echo $row->nama;?></h4>
              </div>
              <div class="modal-body">
                <center>
                  <img src="<?=base_url()?>assets/img/fotoCalonKetua/<?php echo $row->foto;?>" class="img-circle" style="height:35%;width:35%;">
                </center>
                <br><br>
                <?php echo form_open_multipart('CSuperAdmin/UbahFotoCalonKetua');?>
                  <input type="hidden" name="id_ketua" value="<?php echo $row->id_ketua;?>"/>
                  <input type="hidden" name="nama" value="<?php echo $row->nama;?>"/>
                  <input type="hidden" name="foto" value="<?php echo $row->foto;?>"/>
                  <input type="file" name="userfile" size="20" class="form-control" required/>
                  <span class="help-block">Maks dimensi 160 x 160. Tipe file jpeg | jpg | png</span>
                  <br>
                  <button type="submit" class="btn btn-primary">Simpan perubahan</button>
                <?php echo form_close();?>
              </div> 
              <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
              </div>
            </div>          
          </div>
        </div>

        <div class="modal fade" id="hapusKetua<?php echo $row->id_ketua?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Hapus data admin</h4>
              </div>
              <div class="modal-body">
                  Apakah anda ingin menghapus data <?php echo $row->nama ?> ?
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                  <a href="<?=base_url()?>CSuperAdmin/hapusCalonKetua/<?php echo $row->id_ketua; ?>/<?php echo $row->foto; ?>"><button type="button" id="confirmHapus" class="simpan btn btn-primary">Ya</button></a>
               </div>
            </div>          
          </div>
        </div>
      <?php
        }
      ?>













      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; Komputer dan Sistem Informasi 2013.</strong>
      </footer>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url()?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?=base_url()?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?=base_url()?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url()?>assets/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url()?>assets/dist/js/demo.js"></script>
    <!-- page script -->
    <script src="<?=base_url()?>assets/js/validator.js"></script>
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
  </body>
</html>