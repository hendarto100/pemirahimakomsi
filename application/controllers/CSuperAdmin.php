<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CSuperAdmin extends CI_Controller {
	function __construct(){
		parent::__construct();


		$this->load->library('session');
		$this->load->model("Msuperadmin");



	if($this->session->userdata('level')== "Admin" || $this->session->userdata('level')== "")
		{
		redirect('cLoginAdmin')	;
		}



	}

	public function session(){
		$data['nif'] = $this->session->userdata('nif');
		$data['nama'] = $this->session->userdata('nama');
		$data['nama_pengguna']=$this->session->userdata('nama_pengguna');
		$data['kata_sandi']=$this->session->userdata('kata_sandi');
		$data['angkatan'] = $this->session->userdata('angkatan');
		$data['no_hp'] = $this->session->userdata('no_hp');
		$data['level']=$this->session->userdata('level');
		$data['foto']=$this->session->userdata('foto');
		return $data;
	}

	public function perulangan(){

	}
	public function kirimEmail(){
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "ssl://smtp.googlemail.com";
		$config['smtp_port'] = "465";
		$config['smtp_user'] = "kurniawanhendiwijaya@gmail.com";
		$config['smtp_pass'] = "hauntedmanstolid0414";
		$this->load->library('email',$config);
		$this->email->set_newline("\r\n");
		$this->email->from('kurniawanhendiwijaya@gmail.com','hendi');
		$this->email->to('kurniawanhendiw@gmail.com');
		$this->email->cc('kurniawanhendiwijaya@gmail.com');
		$this->email->subject('Coba email');
		$this->email->message('Coba email lewat CodeIgniter');
		if ($this->email->send() == true) {
			echo "Berhasil";
		}else{
			echo $this->email->print_debugger();
			echo "Gagal";
		}

	}

	public function index()
	{
		$data = $this->session();
		$tanggal = getdate();
		$data['pemiraTahunSekarang'] = $this->Msuperadmin->get_pemira_by_tahun($tanggal['year']);
		$this->load->view("vSuperAdmin",$data);

	}

	public function registrasiHakAkses(){
		$nif = $this->input->post('nif');
		$tanggal_mulai = $this->input->post('tanggal_mulai');
		$tanggal_berakhir = $this->input->post('tanggal_berakhir');
		$id_pemira = $this->input->post('id_pemira');

		if(strtotime($tanggal_mulai) <= strtotime(date("Y-m-d")) && strtotime(date("Y-m-d")) <= strtotime($tanggal_berakhir)){
			$hasil = $this->Msuperadmin->cek_pemilih($nif);
			if($hasil->num_rows() != null){
				$this->Msuperadmin->registrasi($id_pemira,$nif);
				$this->session->set_flashdata('sukses',true);
				$data = $this->session();
				$tanggal = getdate();
				$data['pemiraTahunSekarang'] = $this->Msuperadmin->get_pemira_by_tahun($tanggal['year']);
				foreach($hasil->result() as $dataPemilih){
					$data['namaPemilih'] = $dataPemilih->nama;
				}
				$this->load->view("vSuperAdmin",$data);
			}else{
				$this->session->set_flashdata('gagal',true);
				redirect('CSuperAdmin','refresh');
			}
		}else{
			$this->session->set_flashdata('gagalTanggal',true);
			redirect('CSuperAdmin','refresh');
		}
	}

	public function hal_data_pemilih_awal(){
		$data = $this->session();
		$data['pemilih'] = $this->Msuperadmin->get_data_pemilih();
		$this->load->view('vSuperAdminDataPemilihawal',$data);
	}

	public function hal_data_pemilih(){
		$data = $this->session();
		$data['pemilih'] = $this->Msuperadmin->get_data_pemilih();
		$this->load->view('vSuperAdminDataPemilih',$data);
	}

	public function getDataPemilih(){
		$draw=$_REQUEST['draw'];
		$length=$_REQUEST['length'];
		$start=$_REQUEST['start'];
		$search=$_REQUEST['search']["value"];

		$this->db->select('*');
		$this->db->from('pemilih');
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("pemilih.nif",$search);
		$this->db->or_like("nama",$search);
		$this->db->or_like("angkatan",$search);
		$this->db->or_like("status_mahasiswa",$search);
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('nif','asc');
		$this->db->select('*');
		$this->db->from('pemilih');
		$query=$this->db->get();


		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("pemilih.nif",$search);
		$this->db->or_like("nama",$search);
		$this->db->or_like("angkatan",$search);
		$this->db->or_like("status_mahasiswa",$search);

		$this->db->select('*');
		$this->db->from('pemilih');
		$jum=$this->db->get();
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		$nomor_urut=$start+1;
		foreach ($query->result_array() as $dataPemilih) {
			if ($query->num_rows() == null) {
				$output['data'][]=array(
				$nomor_urut," "," "," "," "," ");
			}else{

				if($dataPemilih['status_mahasiswa'] == "Aktif"){
					$status_mahasiswa = "<a href='".base_url()."CSuperAdmin/gantiStatusMahasiswa/Tidak/".$dataPemilih['nif']."' type='button' class='btn btn-success btn-sm'> Aktif</a>";
				}else{
					$status_mahasiswa = "<a href='".base_url()."CSuperAdmin/gantiStatusMahasiswa/Aktif/".$dataPemilih['nif']."' type='button' class='btn btn-danger btn-sm'>Tidak Aktif</a>";
				}
				if($dataPemilih['hak_akses'] == 0){
					$hak_akses = "Tidak";
				}else{
					$hak_akses = "Ya";
				}
				if($dataPemilih['nif'] == $this->session->userdata('nif')){
					$aksi = "<a href='' class='edit-data' data-id='".$dataPemilih['nif']."'><button type='button' class='btn btn-info btn-sm' ><i class='fa fa-pencil'></i></button></a>";
				}else{
					$aksi = "<a href='' class='edit-data' data-id='".$dataPemilih['nif']."'><button type='button' class='btn btn-info btn-sm' ><i class='fa fa-pencil'></i></button></a>
					<a href='' class='hapus-data' data-id='".$dataPemilih['nif']."'><button type='button' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button></a>";
				}

				$output['data'][]=array(
					$nomor_urut,
					$dataPemilih['nif'],
					$dataPemilih['nama'],
					$dataPemilih['angkatan'],
					$status_mahasiswa,
					$hak_akses,
					$aksi
					);
					$nomor_urut++;
			}
		}
		echo json_encode($output);
	}

	public function modalHapusPemilih(){
		foreach ($this->Msuperadmin->cekNif($_POST['id'])->result() as $key) {
			$nama = $key->nama;
		}

		echo "<div class='modal-header'>
              	<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
                <h4 class='modal-title' id='myModalLabel'>Hapus data pemilih</h4>
              </div>
              <div class='modal-body'>
                  Apakah anda ingin menghapus data ".$nama." ?
               </div>
               <div class='modal-footer'>
                  <button type='button' class='btn btn-default' data-dismiss='modal'>Tidak</button>
                  <a href='".base_url()."CSuperAdmin/hapusPemilih/".$_POST['id']."'><button type='button' id='confirmHapus' class='simpan btn btn-primary'>Ya</button></a>
               </div>";
	}

	public function modalEditPemilih(){
		foreach ($this->Msuperadmin->ceknif($_POST['id'])->result() as $row) {
			if($row->hak_akses == false){
				$disabled = "disabled='disabled'";
				$inputHak_akses = "<input type='hidden' name='hak_akses' value='0'>";
				$selectTidak = "selected";
				$selectYa = "";
			}else{
				$disabled = "";
				$inputHak_akses = "";
				$selectTidak = "";
				$selectYa = "selected";
			}
			echo "<div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                <h4 class='modal-title' id='myModalLabel'>Ubah Data Pemilih ".$row->nama."</h4>
              </div>
              <div class='modal-body'>
                <form method='post' action='".base_url()."CSuperAdmin/ubahDataPemilih' data-toggle='validator'>
                  <input type='hidden' name='nifAwal' value='".$row->nif."'>
                  <div class='form-group'>
                    <label>NIF</label>
                    <input type='number' class='form-control' value='".$row->nif."' name='nif' placeholder='NIF' required autofocus>
                  </div><!-- /.form-group -->
                  <div class='form-group'>
                    <label>Nama</label>
                    <input type='text' class='form-control' value='".$row->nama."' name='nama' placeholder='Nama' required>
                  </div><!-- /.form-group -->
                  <div class='form-group'>
                    <label>Angkatan</label>
                    <input type='number' class='form-control' value='".$row->angkatan."' name='angkatan' placeholder='Angkatan' required>
                  </div><!-- /.form-group -->
                    <div class='form-group'>
                      <label>Hak Akses</label>
                      <select class='form-control' name='hak_akses' ".$disabled.">
                        <option value='1' ".$selectYa." >Ya</option>
                        <option value='0' ".$selectTidak." >Tidak</option>
                      </select>
                    </div><!-- /.form-group -->
                  ".$inputHak_akses."
                  <div class='modal-footer'>
                    <button type='button' class='btn btn-danger' data-dismiss='modal'>Batal</button>
                    <button type='submit' class='btn btn-primary'>Simpan Perubahan</button>
                  </div>
                </form>
              </div> ";
		}
	}

	public function hal_data_admin(){
		$data = $this->session();
		$data['error'] = "";
		$data['admin'] = $this->Msuperadmin->get_data_admin();
		$this->load->view('vSuperAdminDataAdmin',$data);
	}

	public function hal_data_pemira(){
		$data = $this->session();
		$data['pemira'] = $this->Msuperadmin->get_data_pemira();
		$data['kandidat'] = $this->Msuperadmin->get_data_calon_ketua();
		$data['calon_kandidat'] = $this->Msuperadmin->get_data_calon_kandidat();
		$data['kandidatPemira'] = $this->Msuperadmin->get_data_kandidat_pemira();
		$data['id_pemira'] = "P".$this->Msuperadmin->generateIdPemira();
		$data['kandidatPemiraTerdaftar'] = $this->Msuperadmin->get_data_calon_kandidat_terdaftar();
		$this->load->view('vSuperAdminDataPemira',$data);
	}

	public function hal_data_pemilih_belum_teregistrasi(){
		$data = $this->session();
		if(null == $this->input->post('tahun')){
			$tanggal = getdate();
			$tahun = $tanggal['year'];
			$data['tahun'] = $tahun;
		}else{
			$tahun = $this->input->post('tahun');
			$data['tahun'] = $tahun;
		}
		$data['tahunPemira'] = $this->Msuperadmin->get_tahun_pemira();
		$this->load->view('vSuperAdminDataPemilihBelumTeregistrasi', $data);
	}

	public function getDataPemilihBelumTeregistrasi($tahun){
		if(null == $tahun){
			$tanggal = getdate();
			$tahun = $tanggal['year'];
		}
		$draw=$_REQUEST['draw'];
		$length=$_REQUEST['length'];
		$start=$_REQUEST['start'];
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select("*");
		$this->db->from("pemilih");
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah
		memasukan keyword didalam filed pencarian*/



		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->select("*");
		$this->db->from("pemilih");
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		if($search!=""){
		$this->db->like("pemilih.nif",$search);
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$this->db->or_like("nama",$search);
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$this->db->or_like("angkatan",$search);
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		}
		$query=$this->db->get();


		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){


		$this->db->select("*");
		$this->db->from("pemilih");
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$this->db->like("pemilih.nif",$search);
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$this->db->or_like("nama",$search);
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$this->db->or_like("angkatan",$search);
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$jum=$this->db->get();
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		$nomor_urut=$start+1;
		foreach ($query->result_array() as $peserta) {
				$output['data'][]=array(
					$nomor_urut,
					$peserta['nif'],
					$peserta['nama'],
					$peserta['angkatan'],
					$peserta['status_mahasiswa']);
				$nomor_urut++;
		}
		echo json_encode($output);
	}

	public function hal_data_pemilih_teregistrasi(){
		$data = $this->session();
		if(null == $this->input->post('tahun')){
			$tanggal = getdate();
			$tahun = $tanggal['year'];
			$data['tahun'] = $tahun;
		}else{
			$tahun = $this->input->post('tahun');
			$data['tahun'] = $tahun;
		}
		$data['tahunPemira'] = $this->Msuperadmin->get_tahun_pemira();
		$this->load->view('vSuperAdminDataPemilihTeregistrasi',$data);
	}

	public function getDataPemilihTeregistrasi($tahun){
		if(null == $tahun){
			$tanggal = getdate();
			$tahun = $tanggal['year'];
		}
		$draw=$_REQUEST['draw'];
		$length=$_REQUEST['length'];
		$start=$_REQUEST['start'];
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select("pemilih.nif, nama, angkatan, status_mahasiswa, status_pilih, year(tanggal_mulai) as tahun_pemira");
		$this->db->from("pemilih");
		$this->db->join("status","status.nif=pemilih.nif");
		$this->db->join("pemira","pemira.id_pemira=status.id_pemira");
		$kondisi = array('year(tanggal_mulai)' => $tahun );
		$this->db->where($kondisi);
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah
		memasukan keyword didalam filed pencarian*/



		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->select("pemilih.nif, nama, angkatan, status_mahasiswa, status_pilih, year(tanggal_mulai) as tahun_pemira");
		$this->db->from("pemilih");
		$this->db->join("status","status.nif=pemilih.nif");
		$this->db->join("pemira","pemira.id_pemira=status.id_pemira");
		$kondisi = array('year(tanggal_mulai)' => $tahun );
		$this->db->where($kondisi);
		if($search!=""){
		$this->db->like("pemilih.nif",$search);
		$this->db->where($kondisi);
		$this->db->or_like("nama",$search);
		$this->db->where($kondisi);
		$this->db->or_like("angkatan",$search);
		$this->db->where($kondisi);
		$this->db->or_like("status_pilih",$search);
		$this->db->where($kondisi);
		}

		$query=$this->db->get();


		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){


		$this->db->select("pemilih.nif, nama, angkatan, status_mahasiswa, status_pilih, year(tanggal_mulai) as tahun_pemira");
		$this->db->from("pemilih");
		$this->db->join("status","status.nif=pemilih.nif");
		$this->db->join("pemira","pemira.id_pemira=status.id_pemira");
		$kondisi = array('year(tanggal_mulai)' => $tahun );
		$this->db->where($kondisi);
		$this->db->like("pemilih.nif",$search);
		$this->db->where($kondisi);
		$this->db->or_like("nama",$search);
		$this->db->where($kondisi);
		$this->db->or_like("angkatan",$search);
		$this->db->where($kondisi);
		$this->db->or_like("status_pilih",$search);
		$this->db->where($kondisi);
		$jum=$this->db->get();
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		$nomor_urut=$start+1;
		foreach ($query->result_array() as $peserta) {
				$output['data'][]=array(
					$nomor_urut,
					$peserta['nif'],
					$peserta['nama'],
					$peserta['angkatan'],
					$peserta['status_mahasiswa'],
					$peserta['status_pilih'],
					$peserta['tahun_pemira']);
				$nomor_urut++;
		}
		echo json_encode($output);
	}

	public function hal_data_calon_ketua(){
		$data = $this->session();
		$data['calonKetua'] = $this->Msuperadmin->get_data_calon_ketua();
		$this->load->view('vSuperAdminDataCalonKetua',$data);
	}

	public function hal_perolehan_suara(){
		$data = $this->session();
		if(null == $this->input->post('tahun')){
			$tanggal = getdate();
			$tahun = $tanggal['year'];
			$data['tahun'] = $tahun;
		}else{
			$tahun = $this->input->post('tahun');
			$data['tahun'] = $tahun;
		}
		$data['perolehanSuara'] = $this->Msuperadmin->get_perolehan_suara($tahun);
		$data['tahunPemira'] = $this->Msuperadmin->get_tahun_pemira();
		$this->load->view('vSuperAdminPerolehanSuara',$data);
	}

	public function hal_riwayat_pemira(){
		$data = $this->session();
		if(null == $this->input->post('tahun')){
			$tanggal = getdate();
			$tahun = $tanggal['year'];
			$data['tahun'] = $tahun;
		}else{
			$tahun = $this->input->post('tahun');
			$data['tahun'] = $tahun;
		}
		$data['perolehanSuara'] = $this->Msuperadmin->get_perolehan_suara($tahun);
		$data['statusPemilih'] = $this->Msuperadmin->get_status_pemilih($tahun);
		$data['tahunPemira'] = $this->Msuperadmin->get_tahun_pemira();
		$this->load->view('vSuperAdminRiwayatPemira',$data);
	}

	public function hapusPemilih(){
		$nif = $this->uri->segment(3);
		$this->Msuperadmin->hapusPemilih($nif);
		$this->session->set_flashdata('berhasilHapusPemilih',true);
		redirect('CSuperAdmin/hal_data_pemilih','refresh');
	}

	public function hapusAdmin(){
		$nif = $this->uri->segment(3);
		$foto = $this->uri->segment(4);

		$this->Msuperadmin->hapusAdmin($nif);
		unlink("./assets/img/fotoAdmin/$foto");
		$this->session->set_flashdata('berhasilHapusAdmin',true);
		redirect('CSuperAdmin/hal_data_admin','refresh');
	}

	public function tambahDataPemilih(){
		$nif = $this->input->post('nif');
		$nama = $this->input->post('nama');
		$angkatan = $this->input->post('angkatan');
		$status_mahasiswa = $this->input->post('status_mahasiswa');
		$ceknif = $this->Msuperadmin->ceknif($nif);
		if($ceknif->num_rows() == null){
			$this->Msuperadmin->tambahDataPemilih($nif,$nama,$angkatan,$status_mahasiswa);
			$this->session->set_flashdata('suksestambah',true);
			redirect('CSuperAdmin/hal_data_pemilih','refresh');
		}else{
			$this->session->set_flashdata('gagaltambah',true);
			redirect('CSuperAdmin/hal_data_pemilih','refresh');
		}
	}

	public function tambahDataAdmin(){
		$nif = $this->input->post('nif');
		$nama = $this->input->post('nama');
		$nama_pengguna = $this->input->post('nama_pengguna');
		$kata_sandi = $this->input->post('kata_sandi');
		$no_hp = $this->input->post('no_hp');
		$level = $this->input->post('level');
		$userfile = $this->input->post('userfile');

		$ceknif = $this->Msuperadmin->cekNifAdmin($nif);
		$cekNamaPengguna = $this->Msuperadmin->cekNamaPengguna($nama_pengguna);

		if($ceknif->num_rows() == null){
			if($cekNamaPengguna->num_rows() == null){
				$this->Msuperadmin->tambahDataAdmin($nif,$nama_pengguna,$kata_sandi,$no_hp,$level);
				$hasilUpload = $this->uploadFoto($nif,$nama,$userfile);
				if($hasilUpload == false){
					$this->Msuperadmin->hapusAdmin($nif);
					$data = $this->session();
					$data['admin'] = $this->Msuperadmin->get_data_admin();
					$data['error'] = $this->upload->display_errors();
		 			$this->session->set_flashdata('uploadFotoGagal',true);
					$this->load->view('vSuperAdminDataAdmin', $data);
				}else{
					$this->session->set_flashdata('suksestambah',true);
					redirect('CSuperAdmin/hal_data_admin','refresh');
				}
			}else{
				$this->session->set_flashdata('namaPenggunaAda',true);
				redirect('CSuperAdmin/hal_data_admin','refresh');
			}
		}else{
			$this->session->set_flashdata('gagaltambah',true);
			redirect('CSuperAdmin/hal_data_admin','refresh');
		}
	}

	public function cari_data_nif_ajax(){
		$mahasiswa = $this->Msuperadmin->cekNif($this->input->post('nif'));
		if($mahasiswa->num_rows() > 0){
			foreach ($mahasiswa->result() as $key) {
				$data = array(
					'nama' => $key->nama,
					'angkatan' => $key->angkatan
				);
			}
		}else{
			$data = array(

				'angkatan' => "Tidak ada data",
				'nama' => "Tidak ada data"
			);
		}

		echo json_encode($data);
	}

	public function tambahDataPemira(){
		$id_pemira = $this->input->post('id_pemira');
		$tanggal_mulai = $this->input->post('tanggal_mulai');
		$tanggal_berakhir = $this->input->post('tanggal_berakhir');
		$calon_kandidat_1 = $this->input->post('calon_kandidat_1');
		$calon_kandidat_2 = $this->input->post('calon_kandidat_2');
		$calon_kandidat_3 = $this->input->post('calon_kandidat_3');

		$cekTahunPemira = $this->Msuperadmin->cekTahunPemira($tanggal_mulai);
		if($cekTahunPemira->num_rows() == null){
			if(strtotime($tanggal_mulai) < strtotime($tanggal_berakhir) && strtotime($tanggal_mulai) >= strtotime(date("Y-m-d")) ){
			$this->Msuperadmin->tambahDataPemira($id_pemira,$tanggal_mulai,$tanggal_berakhir);
			$this->Msuperadmin->tambahDataPemiraKandidat($id_pemira,$calon_kandidat_1,$calon_kandidat_2,$calon_kandidat_3);
			$this->session->set_flashdata('suksestambah',true);
			redirect('CSuperAdmin/hal_data_pemira','refresh');
			}else{
				$this->session->set_flashdata('gagaltambah',true);
				$this->session->set_flashdata('pesanGagal','Gagal tambah data pemira ! Tanggal tidak Sesuai');
				redirect('CSuperAdmin/hal_data_pemira','refresh');
			}
		}else{
			$this->session->set_flashdata('gagaltambah',true);
			$this->session->set_flashdata('pesanGagal','Gagal tambah data pemira ! Tahun pemira sudah ada');
			redirect('CSuperAdmin/hal_data_pemira','refresh');
		}
	}

	public function tambahKandidat(){
		$id_pemira = $this->input->post('id_pemira');
		$tanggal_mulai = $this->input->post('tanggal_mulai');
		$calon_kandidat_1 = $this->input->post('calon_kandidat_1');
		$calon_kandidat_2 = $this->input->post('calon_kandidat_2');
		$calon_kandidat_3 = $this->input->post('calon_kandidat_3');
		if(strtotime($tanggal_mulai) <= strtotime(date("Y-m-d"))){
			$this->session->set_flashdata('gagalTambahKandidat',true);
			redirect('CSuperAdmin/hal_data_pemira','refresh');
		}else{
			$this->Msuperadmin->tambahDataPemiraKandidat($id_pemira,$calon_kandidat_1,$calon_kandidat_2,$calon_kandidat_3);
			$this->session->set_flashdata('suksestambahkandidat',true);
			redirect('CSuperAdmin/hal_data_pemira','refresh');
		}
	}

	public function ubahDataPemira(){
		$tanggal_mulai_awal = $this->input->post('tanggal_mulai_awal');
		$id_pemira = $this->input->post('id_pemira');
		$tanggal_mulai = $this->input->post('tanggal_mulai');
		$tanggal_berakhir = $this->input->post('tanggal_berakhir');
		$calon_kandidat_1_awal = $this->input->post('calon_kandidat_1_awal');
		$calon_kandidat_2_awal = $this->input->post('calon_kandidat_2_awal');
		$calon_kandidat_3_awal = $this->input->post('calon_kandidat_3_awal');
		$calon_kandidat_1 = $this->input->post('calon_kandidat_1');
		$calon_kandidat_2 = $this->input->post('calon_kandidat_2');
		$calon_kandidat_3 = $this->input->post('calon_kandidat_3');
		if(strtotime($tanggal_mulai_awal) <= strtotime(date("Y-m-d"))){
			$this->session->set_flashdata('gagalUbah',true);
			redirect('CSuperAdmin/hal_data_pemira','refresh');
		}else{
			$this->Msuperadmin->ubahDataPemira($id_pemira,$tanggal_mulai,$tanggal_berakhir,$calon_kandidat_1_awal,$calon_kandidat_2_awal,$calon_kandidat_3_awal,$calon_kandidat_1,$calon_kandidat_2,$calon_kandidat_3);
			$this->session->set_flashdata('berhasil',true);
			redirect('CSuperAdmin/hal_data_pemira','refresh');
		}

	}

	public function ubahDataAdmin(){
		$nama_pengguna_awal = $this->input->post('nama_pengguna_awal');
		$nif = $this->input->post('nif');
		$nama = $this->input->post('nama');
		$nama_pengguna = $this->input->post('nama_pengguna');
		$kata_sandi = $this->input->post('kata_sandi');
		$angkatan = $this->input->post('angkatan');
		$no_hp = $this->input->post('no_hp');
		$level = $this->input->post('level');

		$cekNamaPengguna = $this->Msuperadmin->cekNamaPengguna($nama_pengguna);

		if($nama_pengguna == $nama_pengguna_awal){
			$this->Msuperadmin->ubahDataAdmin($nif,$nama_pengguna,$kata_sandi,$no_hp,$level);
			$this->session->set_flashdata('berhasil',true);
			redirect('CSuperAdmin/hal_data_admin','refresh');
		}else{
			if($cekNamaPengguna->num_rows() == null){
				$this->Msuperadmin->ubahDataAdmin($nif,$nama_pengguna,$kata_sandi,$no_hp,$level);
				$this->session->set_flashdata('berhasil',true);
				redirect('CSuperAdmin/hal_data_admin','refresh');
			}else{
				$this->session->set_flashdata('namaPenggunaAdaUbah',true);
				redirect('CSuperAdmin/hal_data_admin','refresh');
			}
		}
	}

	public function uploadFoto($nif,$nama,$userfile){
		$fileName = $nama;
		$config['upload_path'] = './assets/img/fotoAdmin/';
		$config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']	= '1000';
		$config['max_width']  = '160';
		$config['max_height']  = '160';
		$config['file_name'] = $fileName;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload()){
			$hasil = false;
			return $hasil;
		}
		else{
			$data = array('upload_data' => $this->upload->data());
 			$gambar = $this->upload->data();
			$this->Msuperadmin->UploadFoto($nif,$gambar['file_name']);
			$hasil = true;
			return $hasil;
		}
	}

	public function uploadFotoCalonKetua($id_ketua,$nama,$userfile){
		$fileName = $nama;
		$config['upload_path'] = './assets/img/fotoCalonKetua/';
		$config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']	= '1000';
		$config['max_width']  = '160';
		$config['max_height']  = '160';
		$config['file_name'] = $fileName;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload()){
			$hasil = false;
			return $hasil;
		}
		else{
			$data = array('upload_data' => $this->upload->data());
 			$gambar = $this->upload->data();
			$this->Msuperadmin->uploadFotoCalonKetua($id_ketua,$gambar['file_name']);
			$hasil = true;
			return $hasil;
		}
	}

	public function UbahFotoCalonKetua(){
		$id_ketua = $this->input->post('id_ketua');
		$nama = $this->input->post('nama');
		$foto = $this->input->post('foto');
		$userfile = $this->input->post('userfile');
		$hasilUpload = $this->uploadFotoCalonKetua($id_ketua,$nama,$userfile);
		if($hasilUpload == false){
			$data = $this->session();
			$data['admin'] = $this->Msuperadmin->get_data_admin();
			$data['error'] = $this->upload->display_errors();
			$this->session->set_flashdata('uploadFotoGagal',true);
			$this->load->view('vSuperAdminDataCalonKetua', $data);
		}else{
			unlink("./assets/img/fotoCalonKetua/$foto");
			$this->session->set_flashdata('ubahFoto',true);
			redirect('CSuperAdmin/hal_data_calon_ketua','refresh');
		}

	}

	public function UbahFoto(){
		$nif = $this->input->post('nif');
		$nama = $this->input->post('nama');
		$foto = $this->input->post('foto');
		$userfile = $this->input->post('userfile');
		$hasilUpload = $this->uploadFoto($nif,$nama,$userfile);
		if($hasilUpload == false){
			$data = $this->session();
			$data['admin'] = $this->Msuperadmin->get_data_admin();
			$data['error'] = $this->upload->display_errors();
			$this->session->set_flashdata('uploadFotoGagal',true);
			$this->load->view('vSuperAdminDataAdmin', $data);
		}else{
			unlink("./assets/img/fotoAdmin/$foto");
			$this->session->set_flashdata('ubahFoto',true);
			redirect('CSuperAdmin/hal_data_admin','refresh');
		}

	}
	public function gantiStatusMahasiswa($status_mahasiswa){
		$nif = $this->uri->segment(4);
		$data = $this->session();
		if($status_mahasiswa == "Tidak"){
			$status_mahasiswa = "Tidak Aktif";
		}
		$getNama = $this->Msuperadmin->getNamaPemilih($nif);
		foreach ($getNama->result() as $value) {
			$namaPemilih = $value->nama;
		}
		$this->Msuperadmin->gantiStatusMahasiswa($status_mahasiswa,$nif);

		$this->session->set_flashdata('berhasilUbahStatusMahasiswa',true);
		$this->session->set_flashdata('pesanBerhasilUbahStatus',"Ubah data status mahasiswa berhasil ! atas nama ".$namaPemilih."");

		//$data['pemilih'] = $this->Msuperadmin->get_data_pemilih();
		//$this->session->set_flashdata('berhasilUbahStatusMahasiswa',true);
		redirect('CSuperAdmin/hal_data_pemilih','refresh');
	}
	public function ubahDataPemilih(){
		$nifAwal = $this->input->post('nifAwal');
		$nif = $this->input->post('nif');
		$nama = $this->input->post('nama');
		$angkatan = $this->input->post('angkatan');
		$hak_akses = $this->input->post('hak_akses');
		if($nifAwal == $nif){
			if($hak_akses == "1"){
				$hasil = $this->Msuperadmin->ubahDataPemilih($nifAwal,$nif,$nama,$angkatan,$hak_akses);
				if($hasil == true){
					$this->session->set_flashdata('berhasil',true);
					redirect('CSuperAdmin/hal_data_pemilih','refresh');
				}else{
					$this->session->set_flashdata('tidak berhasil',true);
					redirect('CSuperAdmin/hal_data_pemilih','refresh');
				}
			}else if($hak_akses == "0"){
				$cekStatusPilih = $this->Msuperadmin->cekStatusPilih($nif);
				if($cekStatusPilih->num_rows() == null){
					$hasil = $this->Msuperadmin->ubahDataPemilihFalseHapusStatus($nifAwal,$nif,$nama,$angkatan,$hak_akses);
					$data = $this->session();
					if($hasil == true){
						$this->session->set_flashdata('berhasil',true);
						redirect('CSuperAdmin/hal_data_pemilih','refresh');
					}else{
						$this->session->set_flashdata('tidak berhasil',true);
						redirect('CSuperAdmin/hal_data_pemilih','refresh');
					}
				}else{
					$hasil = $this->Msuperadmin->ubahDataPemilihFalse($nifAwal,$nif,$nama,$angkatan,$hak_akses);
					$data = $this->session();
					if($hasil == true){
						$this->session->set_flashdata('berhasil',true);
						redirect('CSuperAdmin/hal_data_pemilih','refresh');
					}else{
						$this->session->set_flashdata('tidak berhasil',true);
						redirect('CSuperAdmin/hal_data_pemilih','refresh');
					}
				}
			}
		}else if($nifAwal != $nif){
			$ceknif = $this->Msuperadmin->ceknif($nif);
			if($ceknif->num_rows() == null){
				if($hak_akses == "1"){
					$hasil = $this->Msuperadmin->ubahDataPemilih($nifAwal,$nif,$nama,$angkatan,$hak_akses);
					if($hasil == true){
						$this->session->set_flashdata('berhasil',true);
						redirect('CSuperAdmin/hal_data_pemilih','refresh');
					}else{
						$this->session->set_flashdata('tidak berhasil',true);
						redirect('CSuperAdmin/hal_data_pemilih','refresh');
					}
				}else if($hak_akses == "0"){
					$cekStatusPilih = $this->Msuperadmin->cekStatusPilih($nif);
					if($cekStatusPilih->num_rows() == null){
						$hasil = $this->Msuperadmin->ubahDataPemilihFalseHapusStatus($nifAwal,$nif,$nama,$angkatan,$hak_akses);
						$data = $this->session();
						if($hasil == true){
							$this->session->set_flashdata('berhasil',true);
							redirect('CSuperAdmin/hal_data_pemilih','refresh');
						}else{
							$this->session->set_flashdata('tidak berhasil',true);
							redirect('CSuperAdmin/hal_data_pemilih','refresh');
						}
					}else{
						$hasil = $this->Msuperadmin->ubahDataPemilihFalse($nifAwal,$nif,$nama,$angkatan,$hak_akses);
						$data = $this->session();
						if($hasil == true){
							$this->session->set_flashdata('berhasil',true);
							redirect('CSuperAdmin/hal_data_pemilih','refresh');
						}else{
							$this->session->set_flashdata('tidak berhasil',true);
							redirect('CSuperAdmin/hal_data_pemilih','refresh');
						}
					}
				}
			}else{
				$this->session->set_flashdata('nifAda',true);
				redirect('CSuperAdmin/hal_data_pemilih','refresh');
			}
		}
	}

	public function tambahDataCalonKetua(){
		$nif = $this->input->post('nif');
		$nama = $this->input->post('nama');
		$angkatan = $this->input->post('angkatan');
		$visi = $this->input->post('visi');
		$misi = $this->input->post('misi');
		$userfile = $this->input->post('userfile');

		$ceknif = $this->Msuperadmin->cekNifCalonKetua($nif);

		if($ceknif->num_rows() == null){
			$this->Msuperadmin->tambahDataCalonKetua($nif,$nama,$angkatan,$visi,$misi);
			$hasilUpload = $this->uploadFotoCalonKetua($nif,$nama,$userfile);
			if($hasilUpload == false){
					$this->Msuperadmin->hapusCalonKetua($nif);
					$data = $this->session();
					$data['calonKetua'] = $this->Msuperadmin->get_data_calon_ketua();
					$data['error'] = $this->upload->display_errors();
		 			$this->session->set_flashdata('uploadFotoGagal',true);
					$this->load->view('vSuperAdminDataCalonKetua', $data);
				}else{
					$this->session->set_flashdata('suksestambah',true);
					redirect('CSuperAdmin/hal_data_calon_ketua','refresh');
				}
		}else{
			$this->session->set_flashdata('gagaltambah',true);
			redirect('CSuperAdmin/hal_data_calon_ketua','refresh');
		}
	}

	public function ubahDataCalonKetua(){
		$nifAwal = $this->input->post('nifAwal');
		$nif = $this->input->post('nif');
		$nama = $this->input->post('nama');
		$angkatan = $this->input->post('angkatan');
		$visi = $this->input->post('visi');
		$misi = $this->input->post('misi');

		if($nifAwal == $nif){
			$this->Msuperadmin->ubahDataCalonKetua($nifAwal,$nif,$nama,$angkatan,$visi,$misi);
			$this->session->set_flashdata('berhasil',true);
			redirect('CSuperAdmin/hal_data_calon_ketua','refresh');
		}else{
			$ceknif = $this->Msuperadmin->cekNifCalonKetua($nif);
			if($ceknif->num_rows() == null){
				$this->Msuperadmin->ubahDataCalonKetua($nifAwal,$nif,$nama,$angkatan,$visi,$misi);
				$this->session->set_flashdata('berhasil',true);
				redirect('CSuperAdmin/hal_data_calon_ketua','refresh');
			}else{
				$this->session->set_flashdata('nifAda',true);
				redirect('CSuperAdmin/hal_data_calon_ketua','refresh');
			}
		}
	}

	public function hapusCalonKetua(){
		$id_ketua = $this->uri->segment(3);
		$foto = $this->uri->segment(4);
		$tanggal_mulai = $this->Msuperadmin->get_tanggal_mulai_pemira_by_id_ketua($id_ketua);
		if($tanggal_mulai->num_rows() != null){
			foreach ($tanggal_mulai->result() as $value) {
				if(strtotime($value->tanggal_mulai) <= strtotime(date("Y-m-d"))){
				$this->session->set_flashdata('gagalHapusCalonKetua',true);
				redirect('CSuperAdmin/hal_data_calon_ketua','refresh');
				}else{
					$this->Msuperadmin->hapusCalonKetua($id_ketua);
					unlink("./assets/img/fotoCalonKetua/$foto");
					$this->session->set_flashdata('berhasilHapusCalonKetua',true);
					redirect('CSuperAdmin/hal_data_calon_ketua','refresh');
				}
			}
		}else{
			$this->Msuperadmin->hapusCalonKetua($id_ketua);
			unlink("./assets/img/fotoCalonKetua/$foto");
			$this->session->set_flashdata('berhasilHapusCalonKetua',true);
			redirect('CSuperAdmin/hal_data_calon_ketua','refresh');
		}
	}

	public function hapusPemira(){
		$id_pemira = $this->uri->segment(3);
		$this->Msuperadmin->hapusPemira($id_pemira);

		$this->session->set_flashdata('berhasilHapusCalonKetua',true);
		redirect('CSuperAdmin/hal_data_pemira','refresh');
	}
}
