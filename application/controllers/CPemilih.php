<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CPemilih extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("Mpemilih");
	}

	public function index()
	{
		$tanggal = getdate();
		$data['pemiraTahunSekarang'] = $this->Mpemilih->get_pemira_by_tahun($tanggal['year']);
		$this->load->view("vLoginPemilih",$data);
	}
	public function session(){
		$data['nif'] = $this->session->userdata('nif');
		$data['nama'] = $this->session->userdata('nama');
		return $data;
	}

	public function hal_pemilih(){
		if(null == $this->session->userdata('nif')){
			redirect('CPemilih');
		}
		$data = $this->session();
		$data['dataCalonKetua'] = $this->Mpemilih->get_calon_ketua();
		$this->load->view("vPemilih",$data);
	}

	public function cek_login(){
		$nif = $this->input->post("nif");
		$tanggal_mulai = $this->input->post("tanggal_mulai");
		$tanggal_berakhir = $this->input->post("tanggal_berakhir");

		if(strtotime($tanggal_mulai) <= strtotime(date("Y-m-d")) && strtotime(date("Y-m-d")) <= strtotime($tanggal_berakhir)){
			$ceknif = $this->Mpemilih->ceknif($nif);
			if($ceknif->num_rows() != null){
				foreach ($ceknif->result() as $sess) {
					$sess_data['nif'] = $sess->nif;
					$sess_data['nama'] = $sess->nama;
					$sess_data['expire'] = time() + (1*60) ;
					$this->session->set_userdata($sess_data);
				}
				//$this->Mpemilih->resetHakAkses($nif);
				redirect('CPemilih/hal_pemilih','refresh');
			}else{
				$this->session->set_flashdata('gagal',true);
				redirect('CPemilih','refresh');
			}
		}else{
			$this->session->set_flashdata('gagal1',true);
			redirect('CPemilih','refresh');
		}
	}

	public function vote($id_ketua){
		$nif = $this->uri->segment(4);
		$this->Mpemilih->vote($id_ketua,$nif);
		$this->session->unset_userdata('nif');
		$this->session->set_flashdata('vote',true);
		$this->Mpemilih->resetHakAkses($nif);
		redirect('CPemilih','refresh');
		session_destroy();
	}
}
