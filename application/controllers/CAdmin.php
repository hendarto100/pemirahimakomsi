<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CAdmin extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');

		if($this->session->userdata('level')=="Super Admin" || $this->session->userdata('level')==""){
			redirect('cLoginAdmin');
	}
		$this->load->model("Madmin");
	}
	public function session(){
		$data['nif'] = $this->session->userdata('nif');
		$data['nama'] = $this->session->userdata('nama');
		$data['nama_pengguna']=$this->session->userdata('nama_pengguna');
		$data['kata_sandi']=$this->session->userdata('kata_sandi');
		$data['angkatan'] = $this->session->userdata('angkatan');
		$data['no_hp'] = $this->session->userdata('no_hp');
		$data['level']=$this->session->userdata('level');
		$data['foto']=$this->session->userdata('foto');
		return $data;
	}
	public function index()
	{

		$data = $this->session();
		$tanggal = getdate();
		$data['pemiraTahunSekarang'] = $this->Madmin->get_pemira_by_tahun($tanggal['year']);
		$this->load->view("vAdmin",$data);

	}

	public function registrasiHakAkses(){
		$nif = $this->input->post('nif');
		$tanggal_mulai = $this->input->post('tanggal_mulai');
		$tanggal_berakhir = $this->input->post('tanggal_berakhir');
		$id_pemira = $this->input->post('id_pemira');

		if(strtotime($tanggal_mulai) <= strtotime(date("Y-m-d")) && strtotime(date("Y-m-d")) <= strtotime($tanggal_berakhir)){
			$hasil = $this->Madmin->cek_pemilih($nif);
			if($hasil->num_rows() != null){
				$this->Madmin->registrasi($id_pemira,$nif);
				$this->session->set_flashdata('sukses',true);
				$data = $this->session();
				$tanggal = getdate();
				$data['pemiraTahunSekarang'] = $this->Madmin->get_pemira_by_tahun($tanggal['year']);
				foreach($hasil->result() as $dataPemilih){
					$data['namaPemilih'] = $dataPemilih->nama;
				}
				$this->load->view("vAdmin",$data);
			}else{
				$this->session->set_flashdata('gagal',true);
				redirect('CAdmin','refresh');
			}
		}else{
			$this->session->set_flashdata('gagalTanggal',true);
			redirect('CAdmin','refresh');
		}
	}

	public function hal_data_pemilih(){
		$data = $this->session();
		$data['pemilih'] = $this->Madmin->get_data_pemilih();
		$this->load->view('vAdminDataPemilih',$data);
	}

	public function getDataPemilih(){
		$draw=$_REQUEST['draw'];
		$length=$_REQUEST['length'];
		$start=$_REQUEST['start'];
		$search=$_REQUEST['search']["value"];

		$this->db->select('*');
		$this->db->from('pemilih');
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("pemilih.nif",$search);
		$this->db->or_like("nama",$search);
		$this->db->or_like("angkatan",$search);
		$this->db->or_like("status_mahasiswa",$search);
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('nif','asc');
		$this->db->select('*');
		$this->db->from('pemilih');
		$query=$this->db->get();


		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("pemilih.nif",$search);
		$this->db->or_like("nama",$search);
		$this->db->or_like("angkatan",$search);
		$this->db->or_like("status_mahasiswa",$search);

		$this->db->select('*');
		$this->db->from('pemilih');
		$jum=$this->db->get();
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		$nomor_urut=$start+1;
		foreach ($query->result_array() as $dataPemilih) {
			if ($query->num_rows() == null) {
				$output['data'][]=array(
				$nomor_urut," "," "," "," "," ");
			}else{

				if($dataPemilih['status_mahasiswa'] == "Aktif"){
					$status_mahasiswa = "<a href='".base_url()."CAdmin/gantiStatusMahasiswa/Tidak/".$dataPemilih['nif']."' type='button' class='btn btn-success btn-sm'> Aktif</a>";
				}else{
					$status_mahasiswa = "<a href='".base_url()."CAdmin/gantiStatusMahasiswa/Aktif/".$dataPemilih['nif']."' type='button' class='btn btn-danger btn-sm'>Tidak Aktif</a>";
				}
				if($dataPemilih['hak_akses'] == 0){
					$hak_akses = "Tidak";
				}else{
					$hak_akses = "Ya";
				}

				$output['data'][]=array(
					$nomor_urut,
					$dataPemilih['nif'],
					$dataPemilih['nama'],
					$dataPemilih['angkatan'],
					$status_mahasiswa,
					$hak_akses,
					"<a href='' class='edit-data' data-id='".$dataPemilih['nif']."'><button type='button' class='btn btn-info btn-sm' ><i class='fa fa-pencil'></i></button></a>"
					);
					$nomor_urut++;
			}
		}
		echo json_encode($output);
	}

	public function hal_data_pemilih_belum_teregistrasi(){
		$data = $this->session();
		if(null == $this->input->post('tahun')){
			$tanggal = getdate();
			$tahun = $tanggal['year'];
			$data['tahun'] = $tahun;
		}else{
			$tahun = $this->input->post('tahun');
			$data['tahun'] = $tahun;
		}
		$data['tahunPemira'] = $this->Madmin->get_tahun_pemira();
		$this->load->view('vAdminDataPemilihBelumTeregistrasi', $data);
	}

	public function getDataPemilihBelumTeregistrasi($tahun){
		if(null == $tahun){
			$tanggal = getdate();
			$tahun = $tanggal['year'];
		}
		$draw=$_REQUEST['draw'];
		$length=$_REQUEST['length'];
		$start=$_REQUEST['start'];
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select("*");
		$this->db->from("pemilih");
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah
		memasukan keyword didalam filed pencarian*/



		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->select("*");
		$this->db->from("pemilih");
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		if($search!=""){
		$this->db->like("pemilih.nif",$search);
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$this->db->or_like("nama",$search);
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$this->db->or_like("angkatan",$search);
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		}
		$query=$this->db->get();


		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){


		$this->db->select("*");
		$this->db->from("pemilih");
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$this->db->like("pemilih.nif",$search);
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$this->db->or_like("nama",$search);
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$this->db->or_like("angkatan",$search);
		$this->db->where("status_mahasiswa","Aktif");
		$this->db->where("nif not in (select nif from status, pemira where pemira.id_pemira=status.id_pemira and year(tanggal_mulai)='$tahun')");
		$jum=$this->db->get();
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		$nomor_urut=$start+1;
		foreach ($query->result_array() as $peserta) {
				$output['data'][]=array(
					$nomor_urut,
					$peserta['nif'],
					$peserta['nama'],
					$peserta['angkatan'],
					$peserta['status_mahasiswa']);
				$nomor_urut++;
		}
		echo json_encode($output);
	}

	public function hal_data_pemilih_teregistrasi(){
		$data = $this->session();
		if(null == $this->input->post('tahun')){
			$tanggal = getdate();
			$tahun = $tanggal['year'];
			$data['tahun'] = $tahun;
		}else{
			$tahun = $this->input->post('tahun');
			$data['tahun'] = $tahun;
		}
		$data['tahunPemira'] = $this->Madmin->get_tahun_pemira();
		$this->load->view('vAdminDataPemilihTeregistrasi',$data);
	}

	public function getDataPemilihTeregistrasi($tahun){
		if(null == $tahun){
			$tanggal = getdate();
			$tahun = $tanggal['year'];
		}
		$draw=$_REQUEST['draw'];
		$length=$_REQUEST['length'];
		$start=$_REQUEST['start'];
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select("pemilih.nif, nama, angkatan, status_mahasiswa, status_pilih, year(tanggal_mulai) as tahun_pemira");
		$this->db->from("pemilih");
		$this->db->join("status","status.nif=pemilih.nif");
		$this->db->join("pemira","pemira.id_pemira=status.id_pemira");
		$kondisi = array('year(tanggal_mulai)' => $tahun );
		$this->db->where($kondisi);
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah
		memasukan keyword didalam filed pencarian*/



		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->select("pemilih.nif, nama, angkatan, status_mahasiswa, status_pilih, year(tanggal_mulai) as tahun_pemira");
		$this->db->from("pemilih");
		$this->db->join("status","status.nif=pemilih.nif");
		$this->db->join("pemira","pemira.id_pemira=status.id_pemira");
		$kondisi = array('year(tanggal_mulai)' => $tahun );
		$this->db->where($kondisi);
		if($search!=""){
		$this->db->like("pemilih.nif",$search);
		$this->db->where($kondisi);
		$this->db->or_like("nama",$search);
		$this->db->where($kondisi);
		$this->db->or_like("angkatan",$search);
		$this->db->where($kondisi);
		$this->db->or_like("status_pilih",$search);
		$this->db->where($kondisi);
		}

		$query=$this->db->get();


		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){


		$this->db->select("pemilih.nif, nama, angkatan, status_mahasiswa, status_pilih, year(tanggal_mulai) as tahun_pemira");
		$this->db->from("pemilih");
		$this->db->join("status","status.nif=pemilih.nif");
		$this->db->join("pemira","pemira.id_pemira=status.id_pemira");
		$kondisi = array('year(tanggal_mulai)' => $tahun );
		$this->db->where($kondisi);
		$this->db->like("pemilih.nif",$search);
		$this->db->where($kondisi);
		$this->db->or_like("nama",$search);
		$this->db->where($kondisi);
		$this->db->or_like("angkatan",$search);
		$this->db->where($kondisi);
		$this->db->or_like("status_pilih",$search);
		$this->db->where($kondisi);
		$jum=$this->db->get();
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		$nomor_urut=$start+1;
		foreach ($query->result_array() as $peserta) {
				$output['data'][]=array(
					$nomor_urut,
					$peserta['nif'],
					$peserta['nama'],
					$peserta['angkatan'],
					$peserta['status_mahasiswa'],
					$peserta['status_pilih'],
					$peserta['tahun_pemira']);
				$nomor_urut++;
		}
		echo json_encode($output);
	}

	public function tambahDataPemilih(){
		$nif = $this->input->post('nif');
		$nama = $this->input->post('nama');
		$angkatan = $this->input->post('angkatan');
		$status_mahasiswa = $this->input->post('status_mahasiswa');
		$ceknif = $this->Madmin->ceknif($nif);
		if($ceknif->num_rows() == null){
			$this->Madmin->tambahDataPemilih($nif,$nama,$angkatan,$status_mahasiswa);
			$this->session->set_flashdata('suksestambah',true);
			redirect('CAdmin/hal_data_pemilih','refresh');
		}else{
			$this->session->set_flashdata('gagaltambah',true);
			redirect('CAdmin/hal_data_pemilih','refresh');
		}
	}

	public function gantiStatusMahasiswa($status_mahasiswa){
		$nif = $this->uri->segment(4);
		$data = $this->session();
		if($status_mahasiswa == "Tidak"){
			$status_mahasiswa = "Tidak Aktif";
		}
		$getNama = $this->Madmin->getNamaPemilih($nif);
		foreach ($getNama->result() as $value) {
			$namaPemilih = $value->nama;
		}
		$this->Madmin->gantiStatusMahasiswa($status_mahasiswa,$nif);

		$this->session->set_flashdata('berhasilUbahStatusMahasiswa',true);
		$this->session->set_flashdata('pesanBerhasilUbahStatus',"Ubah data status mahasiswa berhasil ! atas nama ".$namaPemilih."");

		//$data['pemilih'] = $this->Madmin->get_data_pemilih();
		//$this->session->set_flashdata('berhasilUbahStatusMahasiswa',true);
		redirect('CAdmin/hal_data_pemilih','refresh');
	}

	public function modalHapusPemilih(){
		foreach ($this->Madmin->cekNif($_POST['id'])->result() as $key) {
			$nama = $key->nama;
		}

		echo "<div class='modal-header'>
              	<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
                <h4 class='modal-title' id='myModalLabel'>Hapus data pemilih</h4>
              </div>
              <div class='modal-body'>
                  Apakah anda ingin menghapus data ".$nama." ?
               </div>
               <div class='modal-footer'>
                  <button type='button' class='btn btn-default' data-dismiss='modal'>Tidak</button>
                  <a href='".base_url()."CAdmin/hapusPemilih/".$_POST['id']."'><button type='button' id='confirmHapus' class='simpan btn btn-primary'>Ya</button></a>
               </div>";
	}

	public function modalEditPemilih(){
		foreach ($this->Madmin->ceknif($_POST['id'])->result() as $row) {
			if($row->hak_akses == false){
				$disabled = "disabled='disabled'";
				$inputHak_akses = "<input type='hidden' name='hak_akses' value='0'>";
				$selectTidak = "selected";
				$selectYa = "";
			}else{
				$disabled = "";
				$inputHak_akses = "";
				$selectTidak = "";
				$selectYa = "selected";
			}
			echo "<div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                <h4 class='modal-title' id='myModalLabel'>Ubah Data Pemilih ".$row->nama."</h4>
              </div>
              <div class='modal-body'>
                <form method='post' action='".base_url()."CAdmin/ubahDataPemilih' data-toggle='validator'>
                  <input type='hidden' name='nifAwal' value='".$row->nif."'>
                  <div class='form-group'>
                    <label>NIF</label>
                    <input type='number' class='form-control' value='".$row->nif."' name='nif' placeholder='NIF' required autofocus>
                  </div><!-- /.form-group -->
                  <div class='form-group'>
                    <label>Nama</label>
                    <input type='text' class='form-control' value='".$row->nama."' name='nama' placeholder='Nama' required>
                  </div><!-- /.form-group -->
                  <div class='form-group'>
                    <label>Angkatan</label>
                    <input type='number' class='form-control' value='".$row->angkatan."' name='angkatan' placeholder='Angkatan' required>
                  </div><!-- /.form-group -->
                    <div class='form-group'>
                      <label>Hak Akses</label>
                      <select class='form-control' name='hak_akses' ".$disabled.">
                        <option value='1' ".$selectYa." >Ya</option>
                        <option value='0' ".$selectTidak." >Tidak</option>
                      </select>
                    </div><!-- /.form-group -->
                  ".$inputHak_akses."
                  <div class='modal-footer'>
                    <button type='button' class='btn btn-danger' data-dismiss='modal'>Batal</button>
                    <button type='submit' class='btn btn-primary'>Simpan Perubahan</button>
                  </div>
                </form>
              </div> ";
		}
	}

	public function ubahDataPemilih(){
		$nifAwal = $this->input->post('nifAwal');
		$nif = $this->input->post('nif');
		$nama = $this->input->post('nama');
		$angkatan = $this->input->post('angkatan');
		$status_mahasiswa = $this->input->post('status_mahasiswa');
		$hak_akses = $this->input->post('hak_akses');
		if($nifAwal == $nif){
			if($hak_akses == "1"){
				$hasil = $this->Madmin->ubahDataPemilih($nifAwal,$nif,$nama,$angkatan,$status_mahasiswa,$hak_akses);
				if($hasil == true){
					$this->session->set_flashdata('berhasil',true);
					redirect('CAdmin/hal_data_pemilih','refresh');
				}else{
					$this->session->set_flashdata('tidak berhasil',true);
					redirect('CAdmin/hal_data_pemilih','refresh');
				}
			}else if($hak_akses == "0"){
				$cekStatusPilih = $this->Madmin->cekStatusPilih($nif);
				if($cekStatusPilih->num_rows() == null){
					$hasil = $this->Madmin->ubahDataPemilihFalseHapusStatus($nifAwal,$nif,$nama,$angkatan,$hak_akses);
					$data = $this->session();
					if($hasil == true){
						$this->session->set_flashdata('berhasil',true);
						redirect('CAdmin/hal_data_pemilih','refresh');
					}else{
						$this->session->set_flashdata('tidak berhasil',true);
						redirect('CAdmin/hal_data_pemilih','refresh');
					}
				}else{
					$hasil = $this->Madmin->ubahDataPemilihFalse($nifAwal,$nif,$nama,$angkatan,$hak_akses);
					$data = $this->session();
					if($hasil == true){
						$this->session->set_flashdata('berhasil',true);
						redirect('CAdmin/hal_data_pemilih','refresh');
					}else{
						$this->session->set_flashdata('tidak berhasil',true);
						redirect('CAdmin/hal_data_pemilih','refresh');
					}
				}
			}
		}else if($nifAwal != $nif){
			$ceknif = $this->Madmin->ceknif($nif);
			if($ceknif->num_rows() == null){
				if($hak_akses == "1"){
					$hasil = $this->Madmin->ubahDataPemilih($nifAwal,$nif,$nama,$angkatan,$status_mahasiswa,$hak_akses);
					if($hasil == true){
						$this->session->set_flashdata('berhasil',true);
						redirect('CAdmin/hal_data_pemilih','refresh');
					}else{
						$this->session->set_flashdata('tidak berhasil',true);
						redirect('CAdmin/hal_data_pemilih','refresh');
					}
				}else if($hak_akses == "0"){
					$cekStatusPilih = $this->Madmin->cekStatusPilih($nif);
					if($cekStatusPilih->num_rows() == null){
						$hasil = $this->Madmin->ubahDataPemilihFalseHapusStatus($nifAwal,$nif,$nama,$angkatan,$hak_akses);
						$data = $this->session();
						if($hasil == true){
							$this->session->set_flashdata('berhasil',true);
							redirect('CAdmin/hal_data_pemilih','refresh');
						}else{
							$this->session->set_flashdata('tidak berhasil',true);
							redirect('CAdmin/hal_data_pemilih','refresh');
						}
					}else{
						$hasil = $this->Madmin->ubahDataPemilihFalse($nifAwal,$nif,$nama,$angkatan,$hak_akses);
						$data = $this->session();
						if($hasil == true){
							$this->session->set_flashdata('berhasil',true);
							redirect('CAdmin/hal_data_pemilih','refresh');
						}else{
							$this->session->set_flashdata('tidak berhasil',true);
							redirect('CAdmin/hal_data_pemilih','refresh');
						}
					}
				}
			}else{
				$this->session->set_flashdata('nifAda',true);
				redirect('CAdmin/hal_data_pemilih','refresh');
			}
		}
	}

}
