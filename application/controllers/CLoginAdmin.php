<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CLoginAdmin extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('session');


		$this->load->model("Mloginadmin");
	}

	public function index()
	{
		$this->load->view("vLoginAdmin");
	}

	public function cek_login(){
		$nama = $this->input->post("nama");
		$katasandi = $this->input->post("katasandi");
		$hasil = $this->Mloginadmin->login($nama,$katasandi);
		if($hasil->num_rows() != null){
			foreach ($hasil->result() as $sess) {
				$sess_data['nif'] = $sess->nif;
				$sess_data['nama'] = $sess->nama;
				$sess_data['nama_pengguna'] = $sess->nama_pengguna;
				$sess_data['kata_sandi'] = $sess->kata_sandi;
				$sess_data['angkatan'] = $sess->angkatan;
				$sess_data['no_hp'] = $sess->no_hp;
				$sess_data['level'] = $sess->level;
				$sess_data['foto'] = $sess->foto;
				$sess_data['expire'] = time() + (0.5*60) ;
				$this->session->set_userdata($sess_data);
			}
			if($this->session->userdata('level') == "Super Admin"){


				 echo $this->session->userdata('level');
				 echo " berhasil masuk ";

				 //session_destroy();


				 var_dump($this->session->userdata());

			redirect('cSuperAdmin','refresh');


			}else if($this->session->userdata('level') == "Admin"){

				$sesi_level = $this->session->userdata('level');
				 echo $this->session->userdata('level');
				 echo " berhasil masuk ";

				redirect('cAdmin/index/sesi_level','refresh');
			}
		}else{
			$this->session->set_flashdata('gagal',true);
			echo "Gagal Login";
			//redirect('CLoginAdmin','refresh');

		}
	}

	public function logout(){
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('CLoginAdmin','refresh');
	}
}
