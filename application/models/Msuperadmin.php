<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class MSuperAdmin extends CI_Model {

		function __construct(){
        	parent::__construct();
    	}

    	public function get_data_admin(){
    		$hasil = $this->db->query("select * from pengguna, pemilih where pengguna.nif=pemilih.nif");
    		return $hasil;
    	}

    	public function get_data_pemilih(){
			$hasil = $this->db->query("select pemilih.nif as nif, pemilih.nama, pemilih.angkatan, pemilih.status_mahasiswa, pemilih.hak_akses, status.nif as nifs, status.status_pilih, pemira.tanggal_mulai from pemilih left outer join status on pemilih.nif=status.nif left outer join pemira on status.id_pemira=pemira.id_pemira and year(pemira.tanggal_mulai)=year(now())");
			return $hasil;
		}

		public function get_data_pemira(){
			$hasil = $this->db->query("select * from pemira");
			return $hasil;
		}

		public function get_data_calon_kandidat_terdaftar(){
			$hasil = $this->db->query("select pemira.id_pemira as id_pemira, group_concat(nama) as nama_kandidat from pemira, calon_ketua where pemira.id_pemira=calon_ketua.id_pemira group by pemira.id_pemira;");
			return $hasil;
		}

		public function get_tahun_pemira(){
			$hasil = $this->db->query("select year(tanggal_mulai) as tahun from pemira order by tahun asc");
			return $hasil;
		}

		public function get_tanggal_mulai_pemira_by_id_ketua($id_ketua){
			$hasil = $this->db->query("select tanggal_mulai from calon_ketua, pemira where calon_ketua.id_pemira=pemira.id_pemira and id_ketua='$id_ketua'");
			return $hasil;
		}

		public function get_status_pemilih($tahun){
			$hasil = $this->db->query("select count(nif) as jumlah, status_pilih from status, pemira where status.id_pemira=pemira.id_pemira and year(tanggal_mulai)='$tahun' group by status_pilih order by status_pilih desc");
			return $hasil;
		}

		public function get_perolehan_suara($tahun){
			$hasil = $this->db->query("select count(poling.id_ketua) as poling,poling.id_ketua,calon_ketua.nama from poling,calon_ketua where poling.id_ketua=calon_ketua.id_ketua and year(poling.tanggal)='$tahun' group by poling.id_ketua order by poling desc;");
			return $hasil;
		}

		public function get_pemira_by_tahun($tahun){
			$hasil = $this->db->query("select * from pemira where year(tanggal_mulai)='$tahun'");
			return $hasil;
		}

		public function get_data_pemilih_teregistrasi(){
			$hasil = $this->db->query("select * from pemilih, status, pemira where pemilih.nif=status.nif and status.id_pemira=pemira.id_pemira");
			return $hasil;
		}

		public function get_data_calon_ketua(){
			$hasil = $this->db->query("Select * from calon_ketua");
			return $hasil;
		}

		public function get_data_kandidat_pemira(){
			$hasil = $this->db->query("select * from calon_ketua where id_pemira is not null");
			return $hasil;
		}

		public function get_data_calon_kandidat(){
			$hasil = $this->db->query("select * from calon_ketua where id_pemira is null");
			return $hasil;
		}

		public function hapusPemilih($nif){
			$this->db->query("delete from pemilih where nif='$nif'");
		}

		public function hapusAdmin($nif){
			$this->db->query("delete from pengguna where nif='$nif'");
		}

		public function hapusCalonKetua($nif){
			$this->db->query("delete from calon_ketua where id_ketua='$nif'");
		}

		public function hapusPemira($id_pemira){
			$this->db->query("delete from pemira where id_pemira='$id_pemira'");
		}

		public function cek_pemilih($nif){
			$hasil = $this->db->query("select * from pemilih where nif='$nif' and status_mahasiswa='Aktif' and nif not in (select nif from status, pemira where status.id_pemira=pemira.id_pemira and year(tanggal_mulai)=year(now()))");
			return $hasil;
		}

		public function registrasi($id_pemira,$nif){
			$this->db->query("update pemilih set hak_akses=true where nif='$nif'");
			$this->db->query("insert into status(nif, status_pilih, id_pemira) values('$nif','Belum Pilih','$id_pemira')");
		}

		public function cekNif($nif){
			return $this->db->query("select * from pemilih where nif='$nif'");
		}

		public function cekNifAdmin($nif){
			$hasil = $this->db->query("select * from pengguna where nif='$nif'");
			return $hasil;
		}

		public function cekNifCalonKetua($nif){
			$hasil = $this->db->query("select * from calon_ketua where id_ketua='$nif'");
			return $hasil;
		}


		public function cekNamaPengguna($nama_pengguna){
			$hasil = $this->db->query("select * from pengguna where nama_pengguna='$nama_pengguna'");
			return $hasil;
		}

		public function tambahDataAdmin($nif,$nama_pengguna,$kata_sandi,$no_hp,$level){
			$this->db->query("insert into pengguna(nif,nama_pengguna,kata_sandi,no_hp,level) values('$nif','$nama_pengguna',md5('$kata_sandi'),'$no_hp','$level')");
		}

		public function tambahDataCalonKetua($nif,$nama,$angkatan,$visi,$misi){
			$this->db->query("insert into calon_ketua values('$nif','$nama','$angkatan','$visi','$misi','null',null)");
		}

		public function cekTahunPemira($tanggal_mulai){
			return $this->db->query("select * from pemira where year(tanggal_mulai) = year('$tanggal_mulai')");
		}
		public function tambahDataPemira($id_pemira,$tanggal_mulai,$tanggal_berakhir){
			$this->db->query("insert into pemira values('$id_pemira','$tanggal_mulai','$tanggal_berakhir')");
		}

		public function tambahDataPemiraKandidat($id_pemira,$calon_kandidat_1,$calon_kandidat_2,$calon_kandidat_3){
			$this->db->query("update calon_ketua set id_pemira='$id_pemira' where id_ketua='$calon_kandidat_1'");
			$this->db->query("update calon_ketua set id_pemira='$id_pemira' where id_ketua='$calon_kandidat_2'");
			$this->db->query("update calon_ketua set id_pemira='$id_pemira' where id_ketua='$calon_kandidat_3'");
		}

		public function ubahDataAdmin($nif,$nama_pengguna,$kata_sandi,$no_hp,$level){
			$this->db->query("update pengguna set nama_pengguna='$nama_pengguna',kata_sandi=md5('$kata_sandi'),no_hp='$no_hp',level='$level' where nif='$nif'");
		}

		public function UploadFoto($nif,$gambar){
			$this->db->query("update pengguna set foto='$gambar' where nif='$nif'");
		}

		public function UploadFotoCalonKetua($id_ketua,$gambar){
			$this->db->query("update calon_ketua set foto='$gambar' where id_ketua='$id_ketua'");
		}

		public function ubahDataCalonKetua($nifAwal,$nif,$nama,$angkatan,$visi,$misi){
			$this->db->query("update calon_ketua set id_ketua='$nif', nama='$nama',angkatan='$angkatan',visi='$visi',misi='$misi' where id_ketua='$nifAwal'");
		}

		public function tambahDataPemilih($nif,$nama,$angkatan,$status_mahasiswa){
			$hasil = $this->db->query("insert into pemilih values('$nif','$nama','$angkatan','$status_mahasiswa','false')");
			return $hasil;
		}

		public function getNamaPemilih($nif){
			$hasil = $this->db->query("select nama from pemilih where nif='$nif'");
			return $hasil;
		}

		public function ubahDataPemilih($nifAwal,$nif,$nama,$angkatan,$hak_akses){
			$hasil = $this->db->query("update pemilih set nif='$nif',nama='$nama',angkatan='$angkatan', hak_akses='$hak_akses' where nif='$nifAwal'");
			return $hasil;
		}

		public function cekStatusPilih($nif){
			$hasil = $this->db->query("select * from status, pemira where status.id_pemira=pemira.id_pemira and nif='$nif' and year(tanggal_mulai)=year(now()) and status_pilih='Sudah Pilih'");
			return $hasil;
		}

		public function gantiStatusMahasiswa($status_mahasiswa,$nif){
			$this->db->query("update pemilih set status_mahasiswa='$status_mahasiswa' where nif='$nif'");
		}

		public function ubahDataPemilihFalseHapusStatus($nifAwal,$nif,$nama,$angkatan,$hak_akses){
			$hasil = $this->db->query("update pemilih set nif='$nif',nama='$nama',angkatan='$angkatan', hak_akses='$hak_akses' where nif='$nifAwal'");
			$hasil1 = $this->db->query("delete status.* from status left join pemira on status.id_pemira=pemira.id_pemira where year(tanggal_mulai)=year(now()) and nif='$nifAwal'");
			return $hasil;
		}

		public function ubahDataPemilihFalse($nifAwal,$nif,$nama,$angkatan,$hak_akses){
			$hasil = $this->db->query("update pemilih set nif='$nif',nama='$nama',angkatan='$angkatan', hak_akses='$hak_akses' where nif='$nifAwal'");
			return $hasil;
		}

		public function ubahDataPemira($id_pemira,$tanggal_mulai,$tanggal_berakhir,$calon_kandidat_1_awal,$calon_kandidat_2_awal,$calon_kandidat_3_awal,$calon_kandidat_1,$calon_kandidat_2,$calon_kandidat_3){
			$this->db->query("update pemira set tanggal_mulai='$tanggal_mulai', tanggal_berakhir='$tanggal_berakhir' where id_pemira='$id_pemira'");
			$this->db->query("update calon_ketua set id_pemira = NULL where id_ketua='$calon_kandidat_1_awal' or id_ketua='$calon_kandidat_2_awal' or id_ketua='$calon_kandidat_3_awal'");
			$this->db->query("update calon_ketua set id_pemira = '$id_pemira' where id_ketua='$calon_kandidat_1' or id_ketua='$calon_kandidat_2' or id_ketua='$calon_kandidat_3'");	
		}

		public function generateIdPemira(){
			$uniq = random_string('numeric',2);
			$hasil = $this->db->query("select id_pemira from pemira where id_pemira='$uniq'");
			do{
				$uniq = random_string('numeric',2);
			}while($hasil == $uniq);
			return $uniq;
		}

	}
