<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class MAdmin extends CI_Model {

		function __construct(){
        	parent::__construct();
    	}

		public function cek_pemilih($nif){
			$hasil = $this->db->query("select * from pemilih where nif='$nif' and status_mahasiswa='Aktif' and nif not in (select nif from status, pemira where status.id_pemira=pemira.id_pemira and year(tanggal_mulai)=year(now()))");
			return $hasil;
		}

		public function registrasi($id_pemira,$nif){
			$this->db->query("update pemilih set hak_akses=true where nif='$nif'");
			$this->db->query("insert into status(nif, status_pilih, id_pemira) values('$nif','Belum Pilih','$id_pemira')");
		}

		public function cekStatusPilih($nif){
			$hasil = $this->db->query("select * from status, pemira where status.id_pemira=pemira.id_pemira and nif='$nif' and year(tanggal_mulai)=year(now()) and status_pilih='Sudah Pilih'");
			return $hasil;
		}

		public function get_data_pemilih(){
			$hasil = $this->db->query("select pemilih.nif as nif, pemilih.nama, pemilih.angkatan, pemilih.status_mahasiswa, pemilih.hak_akses, status.nif as nifs, status.status_pilih, pemira.tanggal_mulai from pemilih left outer join status on pemilih.nif=status.nif left outer join pemira on status.id_pemira=pemira.id_pemira and year(pemira.tanggal_mulai)=year(now())");
			return $hasil; 
		}

		public function get_pemira_by_tahun($tahun){
			$hasil = $this->db->query("select * from pemira where year(tanggal_mulai)='$tahun'");
			return $hasil;
		}

		public function get_tahun_pemira(){
			$hasil = $this->db->query("select year(tanggal_mulai) as tahun from pemira order by tahun asc");
			return $hasil;
		}

		public function get_data_pemilih_teregistrasi(){
			$hasil = $this->db->query("select * from pemilih, status, pemira where pemilih.nif=status.nif and status.id_pemira=pemira.id_pemira");
			return $hasil;
		}

		public function ubahDataPemilih($nifAwal,$nif,$nama,$angkatan,$status_mahasiswa,$hak_akses){
			$hasil = $this->db->query("update pemilih set nif='$nif',nama='$nama',angkatan='$angkatan',status_mahasiswa='$status_mahasiswa', hak_akses='$hak_akses' where nif='$nifAwal'");
			return $hasil;
		}

		public function cekNif($nif){
			$hasil = $this->db->query("select * from pemilih where nif='$nif'");
			return $hasil;
		}

		public function getNamaPemilih($nif){
			$hasil = $this->db->query("select nama from pemilih where nif='$nif'");
			return $hasil;
		}

		public function gantiStatusMahasiswa($status_mahasiswa,$nif){
			$this->db->query("update pemilih set status_mahasiswa='$status_mahasiswa' where nif='$nif'");
		}

		public function tambahDataPemilih($nif,$nama,$angkatan,$status_mahasiswa){
			$hasil = $this->db->query("insert into pemilih values('$nif','$nama','$angkatan','$status_mahasiswa','false')");
			return $hasil;
		}

		public function ubahDataPemilihFalseHapusStatus($nifAwal,$nif,$nama,$angkatan,$hak_akses){
			$hasil = $this->db->query("update pemilih set nif='$nif',nama='$nama',angkatan='$angkatan', hak_akses='$hak_akses' where nif='$nifAwal'");
			$hasil1 = $this->db->query("delete from status where nif in (select nif from pemira where year(tanggal_mulai)=year(now()) and nif='$nifAwal')");
			return $hasil;
		}

		public function ubahDataPemilihFalse($nifAwal,$nif,$nama,$angkatan,$hak_akses){
			$hasil = $this->db->query("update pemilih set nif='$nif',nama='$nama',angkatan='$angkatan', hak_akses='$hak_akses' where nif='$nifAwal'");
			return $hasil;
		}
	}
