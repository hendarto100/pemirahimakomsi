<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Mpemilih extends CI_Model {

		function __construct(){
        	parent::__construct();
    	}

		public function login($nif){
			$hasil = $this->db->query("select * from pengguna where nama_pengguna='$nama' and kata_sandi=md5('$katasandi')");
			return $hasil;
		}

		public function get_pemira_by_tahun($tahun){
			$hasil = $this->db->query("select * from pemira where year(tanggal_mulai)='$tahun'");
			return $hasil;
		}

		public function ceknif($nif){
			$hasil = $this->db->query(" select * from pemilih, status, pemira where pemilih.nif=status.nif and status.id_pemira=pemira.id_pemira and status.nif='$nif' and pemilih.hak_akses=true and year(tanggal_mulai)=year(now())");
			return $hasil;
		}

		public function resetHakAkses($nif){
			$this->db->query("update pemilih set hak_akses=false where nif='$nif'");
		}

		public function getPemilih($nif){
			$hasil = $this->db->query("select * from pemilih where nif='$nif'");
			return $hasil;
		}

		public function get_calon_ketua(){
			$hasil = $this->db->query("select id_ketua, nama, visi, misi, foto from calon_ketua, pemira where calon_ketua.id_pemira=pemira.id_pemira and year(tanggal_mulai)=year(now())");
			return $hasil;
		}

		public function vote($id_ketua,$nif){
			$this->db->query("update status set status_pilih='Sudah Pilih' where nif='$nif'");
			$this->db->query("insert into poling values('$id_ketua',now(),now())");				
		}
	}
