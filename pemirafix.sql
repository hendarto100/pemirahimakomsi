-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2018 at 06:39 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pemirafix`
--

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `nif` varchar(10) DEFAULT NULL,
  `nama_pengguna` varchar(25) DEFAULT NULL,
  `kata_sandi` varchar(50) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `level` varchar(15) DEFAULT NULL,
  `foto` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`nif`, `nama_pengguna`, `kata_sandi`, `no_hp`, `level`, `foto`) VALUES
('03281', 'hendi', 'e00cf25ad42683b3df678c61f42c6bda', '085642943447', 'Super Admin', 'Kurniawan_Hendi_Wijaya.jpg'),
('03205', 'nadhia', 'e00cf25ad42683b3df678c61f42c6bda', '085735390019', 'Super Admin', 'Nadhia_Nuri_Tariana.JPG'),
('03119', 'deny', 'e00cf25ad42683b3df678c61f42c6bda', '087889456255', 'Admin', 'Deny_Adiyasa.jpg'),
('04096', 'desti', 'e00cf25ad42683b3df678c61f42c6bda', '085867682336', 'Admin', 'Amalia_Destiana.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD KEY `pengguna` (`nif`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD CONSTRAINT `pengguna_ibfk_1` FOREIGN KEY (`nif`) REFERENCES `pemilih` (`nif`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
